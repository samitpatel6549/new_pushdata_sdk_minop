using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using Microsoft.VisualBasic;
using System.Diagnostics;
using System.IO;
using System.IO.Ports;
using System.Security;
using System.Reflection;
using System.Management;
using System.Runtime.InteropServices;
using System.Configuration;
using System.Net;
using SemacV14;
using SemacV14.Service;
using System.Timers;
using Newtonsoft.Json;



namespace SaaSPushTech
{
    public partial class FrmLog : Form
    {

        #region "Declaration"

        //private int fnCount;
        //private int TransCount = 0;
        private bool fbOpenFlag;

        public delegate void delegateAppendMessage(string message);
        public delegateAppendMessage theDelegateAppendMessage;
        public static FrmLog theForm;	// Global form object

        LogServer theLogServer;	// Log server
        Boolean stopping;		// Is stopping?
        ManualResetEvent stoppedEvent;//stop event

        enum ThreadState { RUNNING, STOPPED };
        ThreadState watchState;

        //BioWeb 
        public delegate void TerminalConnectHandler(SemacV14.Entity.TerminalEntity En);
        public delegate void ExecuteHandler(SemacV14.Service.ExecuteArgz Ea);

        private System.Timers.Timer tmrPerformSayHello = null;
        public static DataTable dtDeviceInfo;
        public delegate void delegateAddEvent(string msg);
        #endregion

        public FrmLog()
        {
            InitializeComponent();

            // Initialize objects
            theDelegateAppendMessage = new delegateAppendMessage(AppendMessage);
            theForm = this;
            watchState = ThreadState.STOPPED;
        }

        #region "Form Events"

        private void FrmLog_Load(object sender, EventArgs e)
        {
            try
            {
                //Terminal tl = new Terminal();
                //tl.PushDatasendSMS(1,Convert.ToDateTime(DateTime.Now));

                //Read Local Connection String from Application Path and Update Config
                string ConnStr = "";
                //ConnStr = ReadConnStrLocal();
                //if (ConnStr != "")
                //{
                //    //UpdateConnectionConfig("Server", ConnStr);
                //    //ClsGlobal.LocalConStr = ConnStr;
                //    ClsGlobal.LocalConStrMySql = ConnStr;
                //}

                fbOpenFlag = false;
                FuncEnabledCommand(true);
                Application.DoEvents();
                //int val = 100;
                //int div = 0;
                //int resultVal;
                //resultVal = (val / div);


                //Open Network
                cmdOpenNetWork_Click(null, null);
                //this.AxFPCLOCK_Svr1.OpenNetwork(1018);
                //BioWeb
                //Create Device DataTable
                //dtDeviceInfo = new DataTable();
                //dtDeviceInfo.Columns.Add("TerminalID");
                //dtDeviceInfo.Columns.Add("ChannelID");
                //dtDeviceInfo.Columns.Add("SerialNumber");
                //dtDeviceInfo.AcceptChanges();
                //tmrPerformSayHello = new System.Timers.Timer();
                //tmrPerformSayHello.Interval = 50000;
                ////tmrPerformSayHello.Interval = 120000;
                //tmrPerformSayHello.Enabled = true;
                //tmrPerformSayHello.Elapsed += new ElapsedEventHandler(tmrPerformSayHello_Elapsed);
                //BeginListen();
            }
            catch (Exception ex)
            {
                ClsGlobal.WriteErrorLog("FrmLog_Load : " + ex.Message);
            }
        }
        private void FrmLog_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                this.Hide();
                e.Cancel = true;

                //BioFace
                //if (watchState != ThreadState.STOPPED)
                //    StopWatchEventThread();
            }
            catch
            {
            }
        }
        private void FrmLog_FormClosed(object sender, FormClosedEventArgs e)
        {

        }

        #endregion

        #region "Functions"

        private string ReadConnStrLocal()
        {
            try
            {
                //string[] lines = System.IO.File.ReadAllLines(Application.StartupPath + "\\SQLCon.Mt");
                string[] lines = System.IO.File.ReadAllLines(Application.StartupPath + "\\MySQLCon.Mt");
                string line0 = lines[0];
                return line0;
            }
            catch (Exception ex)
            {
                ClsGlobal.WriteErrorLog("ReadConnStrLocal : " + ex.Message);
                return "";
            }
        }

        #endregion

        #region "BioT5"

        private void FuncEnabledCommand(bool abEnableFlag)
        {
            try
            {
                cmdOpenNetWork.Enabled = abEnableFlag;
                cmdCloseNetWork.Enabled = !abEnableFlag;
            }
            catch (Exception ex)
            {
                ClsGlobal.WriteErrorLog("FuncEnabledCommand : " + ex.Message);
            }
        }
        private int funcSaveGeneralLogData(ref int anCount, ref int aSEnrollNumber, ref int aVerifyMode, ref int aInOutMode, ref DateTime adwDate, ref bool abDrawFlag,
            ref string astrRemoteIP, ref int anRemotePort, ref int anDeviceId, ref string anSerialNo)
        {
            try
            {
                Int32 retVal = 0;
                ClsBALTblTransaction ObjClsBALTblTransaction = new ClsBALTblTransaction();

                if (aSEnrollNumber != 0)
                {
                    // TransCount = TransCount + 1;
                    //lblTotal.Text = "Trans Count : " + Convert.ToString(TransCount);
                }

                // Assin Local Variable
                int _anCount = anCount;
                int _aSEnrollNumber = aSEnrollNumber;
                int _aVerifyMode = aVerifyMode;
                int _aInOutMode = aInOutMode;
                DateTime _adwDate = adwDate;
                bool _abDrawFlag = abDrawFlag;
                string _astrRemoteIP = astrRemoteIP.Replace(" ", "");
                int _anRemotePort = anRemotePort;
                int _anDeviceId = anDeviceId;
                string _anSerialNo = anSerialNo;

                System.Windows.Forms.Application.DoEvents();

                if (anCount >= 1)
                {
                    string VInOut = null;
                    System.DateTime PunchTime = default(System.DateTime);

                    switch (aInOutMode)
                    {
                        case ClsGlobal.LOG_IOMODE_IN:
                            VInOut = "IN";
                            break;
                        case ClsGlobal.LOG_IOMODE_OUT:
                            VInOut = "OUT";
                            break;
                        case ClsGlobal.LOG_IOMODE_OVER_IN:
                            VInOut = "IN";
                            break;
                        case ClsGlobal.LOG_IOMODE_OVER_OUT:
                            VInOut = "OUT";
                            break;
                        default:
                            VInOut = "IN";
                            break;
                    }

                    PunchTime = adwDate;

                    //Save Transaction in Text File
                    string Trans = "";
                    if (_aSEnrollNumber != 0)
                    {
                        Trans = aSEnrollNumber + "," + PunchTime.ToString("yyyy-MM-dd HH:mm:ss") + "," + VInOut + "," + astrRemoteIP.Replace(" ", "") + "," + anRemotePort + "," + anDeviceId + "," + System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                        ClsGlobal.WriteAllTrans(Trans);
                    }

                    string ErrorMsg = "";

                    if (_aSEnrollNumber == 0)
                    {

                        ObjClsBALTblTransaction.SayHello(anSerialNo, ref ErrorMsg);
                        return 1;
                    }
                    else
                    {
                        //Insert Success Transaction - SQL
                        ObjClsBALTblTransaction = new ClsBALTblTransaction();
                        ObjClsBALTblTransaction.EnrollNo = _aSEnrollNumber;
                        ObjClsBALTblTransaction.InOutMode = VInOut;
                        ObjClsBALTblTransaction.PunchTime = Convert.ToDateTime(PunchTime.ToString("yyyy-MM-dd HH:mm:ss"));
                        ObjClsBALTblTransaction.DeviceIP = _astrRemoteIP.Replace(" ", "");
                        ObjClsBALTblTransaction.DeviceID = _anDeviceId;
                        ObjClsBALTblTransaction.DeviceSerialNo = _anSerialNo;
                        retVal = ObjClsBALTblTransaction.OnSave(ref ErrorMsg);
                    }
                    if (_aSEnrollNumber != 0)
                    {
                        if (retVal == 1)
                        {
                            ClsGlobal.TransCount = ClsGlobal.TransCount + 1;
                            lblTotal.Text = "Trans Count : " + Convert.ToString(ClsGlobal.TransCount);
                            ClsGlobal.WriteSuccessTrans(Trans);

                            //Terminal tl = new Terminal();
                            //tl.PushDatasendSMS(_aSEnrollNumber, Convert.ToDateTime(PunchTime.ToString("yyyy-MM-dd HH:mm:ss")));
                        }
                        else if (retVal != 1 && retVal != 2)
                        {
                            ClsGlobal.WriteFailTrans(Trans);
                        }
                    }

                }
                return retVal;
            }
            catch (Exception ex)
            {
                ClsGlobal.WriteErrorLog("funcSaveGeneralLogData : " + ex.Message);
                return -1;
            }
        }
        //private void axFPCLOCK_Svr1_OnReceiveGLogData(object sender, AxFPCLOCK_SVRLib._DFPCLOCK_SvrEvents_OnReceiveGLogDataEvent e)
        //{
        //    try
        //    {
        //        int result = 0;

        //        long vnResultCode = 0;
        //        int sendindex = 0;
        //        int senddeviceid = 0;
        //        int sendenrollID = 0;

        //        sendindex = e.linkindex;
        //        senddeviceid = e.vnDeviceID;
        //        sendenrollID = e.anSEnrollNumber;
        //        result = 1;
        //        fnCount = fnCount + 1;


        //        bool flag = true;
        //        string li = e.linkindex.ToString();
        //        Int32 RetVal = 0;
        //        RetVal = funcSaveGeneralLogData(ref fnCount, ref e.anSEnrollNumber, ref e.anVerifyMode, ref e.anInOutMode, ref e.anLogDate, ref flag, ref e.astrDeviceIP, ref e.anDevicePort, ref e.vnDeviceID, ref li);

        //        // 1 For New and 2 For Second Time

        //        if (RetVal == 1 || RetVal == 2)
        //        {
        //            vnResultCode = AxFPCLOCK_Svr1.SendResultandTime(sendindex, senddeviceid, sendenrollID, result);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ClsGlobal.WriteErrorLog("axFPCLOCK_Svr1_OnReceiveGLogData : " + ex.Message);

        //        if (e.anSEnrollNumber != 0)
        //        {
        //            string VInOut = "";
        //            switch (e.anInOutMode)
        //            {
        //                case ClsGlobal.LOG_IOMODE_IN:
        //                    VInOut = "IN";
        //                    break;
        //                case ClsGlobal.LOG_IOMODE_OUT:
        //                    VInOut = "OUT";
        //                    break;
        //                case ClsGlobal.LOG_IOMODE_OVER_IN:
        //                    VInOut = "Over IN";
        //                    break;
        //                case ClsGlobal.LOG_IOMODE_OVER_OUT:
        //                    VInOut = "Over OUT";
        //                    break;
        //                default:
        //                    VInOut = "--";
        //                    break;
        //            }
        //            string Trans = e.anSEnrollNumber + e.anLogDate.ToString("yyyy-MM-dd HH:mm:ss") + "," + "," + VInOut + "," + e.astrDeviceIP.Replace(" ", "") + "," + e.anDevicePort + "," + e.vnDeviceID + "," + System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        //            ClsGlobal.WriteFailTrans(Trans);
        //        }
        //    }
        //}

        #endregion

        #region "BioFace"

        // Append a message into list
        public void AppendMessage(string message)
        {
            lblTotal.Text = "Trans Count : " + message;
        }
        public void OnAddEvent(String msg)
        {
            //lstEvents.Items.Add(msg);
            //lblEventCount.Text = lstEvents.Items.Count.ToString();
        }

        public Boolean OnTimeLog(
          String terminalType,
          Int32 terminalID,
          String serialNumber,
          Int32 transactionID,
          DateTime logTime,
          Int64 userID,
          Int32 doorID,
          String attendanceStatus,
          String verifyMode,
          Int32 jobCode,
          String antipass,
          Byte[] photo)
        {
            String msg;

            try
            {
                msg = "[" + terminalType + ":";
                msg += terminalID.ToString();
                msg += " SN=" + serialNumber + "] ";
                msg += "TimeLog";
                msg += "(" + transactionID.ToString() + ") ";
                msg += logTime.ToString() + ", ";
                msg += "UserID=" + String.Format("{0}, ", userID);
                msg += "Door=" + doorID.ToString() + ", ";
                msg += "AttendStat=" + attendanceStatus + ", ";
                msg += "VMode=" + verifyMode + ", ";
                msg += "JobCode=" + jobCode.ToString() + ", ";
                msg += "Antipass=" + antipass + ", ";
                if (photo == null)
                    msg += "Photo=No";
                else
                {
                    msg += "Photo=Yes ";
                    msg += "(" + Convert.ToString(photo.Length) + "bytes)";
                }
                BeginInvoke(new delegateAddEvent(OnAddEvent), msg);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public Boolean OnAdminLog(
            String terminalType,
            Int32 terminalID,
            String serialNumber,
            Int32 transactionID,
            DateTime logTime,
            Int64 adminID,
            Int64 userID,
            String action,
            Int32 result)
        {
            String msg;

            try
            {
                msg = "[" + terminalType + ":";
                msg += terminalID.ToString();
                msg += " SN=" + serialNumber + "] ";
                msg += "AdminLog";
                msg += "(" + transactionID.ToString() + ") ";
                msg += logTime.ToString() + ", ";
                msg += "AdminID=" + String.Format("{0}, ", adminID);
                msg += "UserID=" + String.Format("{0}, ", userID);
                msg += "Action=" + action + ", ";
                msg += "Status=" + String.Format("{0:D}", result);
                BeginInvoke(new delegateAddEvent(OnAddEvent), msg);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public Boolean OnAlarmLog(
            String terminalType,
            Int32 terminalID,
            String serialNumber,
            Int32 transactionID,
            DateTime logTime,
            Int64 userID,
            Int32 doorID,
            String alarmType)
        {
            String msg;
            try
            {
                msg = "[" + terminalType + ":";
                msg += terminalID.ToString();
                msg += " SN=" + serialNumber + "] ";
                msg += "AlarmLog";
                msg += "(" + transactionID.ToString() + ") ";
                msg += logTime.ToString() + ", ";//'at'
                msg += "UserID=" + String.Format("{0}, ", userID);
                msg += "Door=" + doorID.ToString() + ", ";
                msg += "Type=" + alarmType;
                BeginInvoke(new delegateAddEvent(OnAddEvent), msg);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public void OnPing(
            String terminalType,
            Int32 terminalID,
            String serialNumber,
            Int32 transactionID)
        {
            String msg;

            msg = "[" + terminalType + ":";
            msg += terminalID.ToString();
            msg += " SN=" + serialNumber + "] ";
            msg += "KeepAlive";
            msg += "(" + transactionID.ToString() + ") ";
            BeginInvoke(new delegateAddEvent(OnAddEvent), msg);
        }
        //private void MonitoringThread(object state)
        //{
        //    UInt16 portNum = Convert.ToUInt16(txtPortNo.Text); // Get port number.

        //    m_LogServer = new LogServer(portNum, OnTimeLog, OnAdminLog, OnAlarmLog, OnPing);   // Create and start log server.

        //    // Watch stop signal.
        //    while (m_Running)
        //    {
        //        Thread.Sleep(100);  // Simulate some lengthy operations.
        //    }

        //    m_LogServer.Dispose();  // Dispose log server
        //    m_StopEvent.Set();      // Signal the stopped event.
        //}
        private void WatchEventThread(object state)
        {
            try
            {
                UInt16 portNum = Convert.ToUInt16(ClsGlobal.ServerPort_BioFace);	// Get port number.
                theLogServer = new LogServer(portNum, OnTimeLog, OnAdminLog, OnAlarmLog, OnPing);	// Create and start log server.

                // Watch stop signal.
                while (!this.stopping)
                {
                    Thread.Sleep(100);  // Simulate some lengthy operations.
                }

                theLogServer.Dispose();	// Dispose log server
                this.stoppedEvent.Set();// Signal the stopped event.
            }
            catch (Exception ex)
            {
                ClsGlobal.WriteErrorLog("WatchEventThread : BioFace : " + ex.Message);
            }

        }
        private void StartWatchEventThread()
        {
            try
            {
                this.stopping = false;
                this.stoppedEvent = new ManualResetEvent(false);

                if (ThreadPool.QueueUserWorkItem(new WaitCallback(WatchEventThread)))
                    watchState = ThreadState.RUNNING;
            }
            catch (Exception ex)
            {
                ClsGlobal.WriteErrorLog("StartWatchEventThread : BioFace : " + ex.Message);
            }

        }
        private void StopWatchEventThread()
        {
            try
            {
                this.stopping = true;
                this.stoppedEvent.WaitOne();

                watchState = ThreadState.STOPPED;
            }
            catch (Exception ex)
            {
                ClsGlobal.WriteErrorLog("StopWatchEventThread : BioFace : " + ex.Message);
            }
        }

        #endregion

        #region "BioWeb"

        private void BeginListen()
        {
            try
            {
                //Connected
                SemacV14.Service.CurrentService.OnConnected += this.OnConnected;
                //DisConnected
                SemacV14.Service.CurrentService.OnDisconnected += this.OnDisconnected;
                //Start TCP Listen
                SemacV14.Service.CurrentService.StartListen(Convert.ToUInt16(ClsGlobal.ServerPort_BioWeb));    //eg:listen Port 1018

                //SemacV14.Service.CurrentService.StartListen(Convert.ToUInt16(2000));
                //Realtime Transaction
                SemacV14.Service.CurrentService.OnRealtimeTransaction += this.HandleRealtime;
                //Realtime Transaction
                SemacV14.Service.CurrentService.OffLineLogTransaction += this.HandleRealtime; // same handle with RealtimeTransaction
            }
            catch (Exception ex)
            {
                ClsGlobal.WriteErrorLog("BeginListen : BioWeb : " + ex.Message);
            }


        }
        private void tmrPerformSayHello_Elapsed(object sender, ElapsedEventArgs e)
        {
            try
            {
                tmrPerformSayHello.Enabled = false;
                if (dtDeviceInfo.Rows.Count > 0)
                {
                    foreach (DataRow dr in dtDeviceInfo.Rows)
                    {
                        if (!string.IsNullOrEmpty(dr["ChannelID"].ToString().Trim())
                                  && !string.IsNullOrEmpty(dr["TerminalID"].ToString().Trim())
                                  && !string.IsNullOrEmpty(dr["SerialNumber"].ToString().Trim()))
                        {
                            string ErrorMsg = "";
                            ClsBALTblTransaction objBAL = new ClsBALTblTransaction();
                            objBAL.DeviceID = Convert.ToInt64(dr["TerminalID"].ToString().Trim());
                            objBAL.DeviceSerialNo = dr["SerialNumber"].ToString().Trim();
                            objBAL.SayHello(objBAL.DeviceSerialNo, ref ErrorMsg);
                            if (!string.IsNullOrEmpty(ErrorMsg.Trim()))
                            {
                                ClsGlobal.WriteErrorLog(" SayHello : BioWeb : " + ErrorMsg.ToString());
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ClsGlobal.WriteErrorLog(" tmrPerformSayHello : BioWeb : " + ex.ToString());
            }
            finally
            {
                tmrPerformSayHello.Enabled = true;
            }


        }
        public void UpdateChannelID(string TerminalID, string ChannelID, string SerialNumber)
        {
            try
            {
                lock (dtDeviceInfo.Rows.SyncRoot)
                {
                    if (dtDeviceInfo != null)
                    {
                        if (dtDeviceInfo.Rows.Count > 0)
                        {
                            foreach (DataRow DR in dtDeviceInfo.Rows)
                            {
                                if (DR["ChannelID"].ToString() == ChannelID.Trim())
                                {
                                    dtDeviceInfo.Rows.Remove(DR);
                                }
                            }
                            dtDeviceInfo.AcceptChanges();
                            dtDeviceInfo.Rows.Add(TerminalID, ChannelID, SerialNumber);
                            dtDeviceInfo.AcceptChanges();

                        }
                        else
                        {
                            dtDeviceInfo.Rows.Add(TerminalID, ChannelID, SerialNumber);
                            dtDeviceInfo.AcceptChanges();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ClsGlobal.WriteErrorLog(" UpdateChannelID : BioWeb : " + ex.ToString());
            }
        }
        //OnConnected
        private void OnConnected(SemacV14.Entity.TerminalEntity En)
        {
            try
            {
                if (!string.IsNullOrEmpty(En.ChannelID.ToString().Trim())
                    && !string.IsNullOrEmpty(En.TerminalID.ToString().Trim())
                    && !string.IsNullOrEmpty(En.Status.ToString().Trim())
                    && !string.IsNullOrEmpty(En.IPAddress.ToString().Trim())
                    && !string.IsNullOrEmpty(En.MacAddress.ToString().Trim())
                    && !string.IsNullOrEmpty(En.SerialNo.ToString().Trim())
                    && !string.IsNullOrEmpty(En.ModelName.ToString().Trim())
                    && !string.IsNullOrEmpty(En.RegisteredUserNo.ToString().Trim())
                    && !string.IsNullOrEmpty(En.RegisteredUserNo.ToString().Trim())
                    )
                {
                    //Update Device Channel ID

                    string macadd = En.MacAddress.Replace("-", "");
                    En.SerialNo = macadd.Substring(macadd.Length - 6);
                    UpdateChannelID(En.TerminalID.ToString(), En.ChannelID, En.SerialNo);
                }
            }
            catch (Exception ex)
            {
                ClsGlobal.WriteErrorLog("OnConnected : BioWeb :  " + ex.ToString());
            }

        }
        //OnDisconnected
        private void OnDisconnected(SemacV14.Entity.TerminalEntity En)
        {
            try
            {
                string DisconnectingChannelID = En.ChannelID;
                string TerminalID = En.TerminalID.ToString();
                DataTable tmpDt = dtDeviceInfo.Copy();
                //Delete Device Entry from Global Table when device Disconnects.
                for (int i = tmpDt.Rows.Count - 1; i >= 0; i--)
                {
                    if (tmpDt.Rows[i]["ChannelID"].ToString() == DisconnectingChannelID)
                    {
                        tmpDt.Rows[i].Delete();
                        tmpDt.AcceptChanges();
                    }
                }
                dtDeviceInfo.Clear();
                dtDeviceInfo = tmpDt.Copy();
            }
            catch (Exception ex)
            {
                ClsGlobal.WriteErrorLog("OnDisconnected : BioWeb : For ChannelID : " + En.ChannelID.ToString() + " TerminalID : " + En.TerminalID + " Error : " + ex.Message);
            }



        }
        // private async void AddProduct()
        //{           
        //     Product p = new Product();
        //     p.Id = 3;
        //     p.Name = "Rolex";
        //     p.Category = "Watch";
        //     p.Price = 1299936;
        //     using (var client = new HttpClient())
        //     {
        //         var serializedProduct = JsonConvert.SerializeObject(p);
        //         var content = new StringContent(serializedProduct, Encoding.UTF8, "application/json");
        //         var result = await client.PostAsync(URI, content);
        //     }

        // }
        //Realtime
        private void HandleRealtime(SemacV14.Service.ExecuteArgz Ea)
        {
            try
            {
                if (!(Ea.EntityList == null))
                {
                    foreach (SemacV14.Entity.DoorLogEntity En in Ea.EntityList)
                    {
                        if ((En.VerificationSource.ToUpper() == "CARD" ||
                              En.VerificationSource.ToUpper() == "FINGERPRINT" ||
                              En.VerificationSource.ToUpper().Contains("PASSWORD"))
                             && En.EventAlarmCode.ToUpper() == "NONE" && En.UserID > 0)
                        {

                            string VInOut = "", Trans = "", ErrorMsg = "";
                            int retVal = 0;
                            if (En.InOutIndication.ToUpper().Contains("IN")) //ACCESS IN DURING NORMAL STATE
                            {
                                VInOut = "IN";
                            }
                            else if (En.InOutIndication.ToUpper().Contains("OUT")) //ACCESS OUT DURING NORMAL STATE
                            {
                                VInOut = "OUT";
                            }
                            else
                            {
                                VInOut = "IN";
                            }
                            //SUCCESS PUNCHES

                            Trans = En.UserID.ToString() + "," + En.EntryDate.ToString("yyyy-MM-dd HH:mm:ss") + "," + VInOut + "," + En.TerminalID.ToString() + "," + System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                            ClsGlobal.WriteAllTrans(Trans);

                            ClsBALTblTransaction ObjClsBALTblTransaction = new ClsBALTblTransaction();
                            ObjClsBALTblTransaction.EnrollNo = Convert.ToInt64(En.UserID.ToString());
                            ObjClsBALTblTransaction.InOutMode = VInOut;
                            ObjClsBALTblTransaction.PunchTime = Convert.ToDateTime(En.EntryDate.ToString("yyyy-MM-dd HH:mm:ss"));
                            ObjClsBALTblTransaction.DeviceIP = "127.0.0.1";
                            ObjClsBALTblTransaction.DeviceID = Convert.ToInt32(En.TerminalID);
                            //ObjClsBALTblTransaction.DeviceSerialNo="";
                            ObjClsBALTblTransaction.DeviceSerialNo = dtDeviceInfo.Rows[0]["SerialNumber"].ToString().Trim();
                            retVal = ObjClsBALTblTransaction.OnSave(ref ErrorMsg);

                            if (retVal == 1)
                            {
                                ClsGlobal.TransCount = ClsGlobal.TransCount + 1;
                                lblTotal.Text = "Trans Count : " + Convert.ToString(ClsGlobal.TransCount);
                            }
                            else
                            {
                                ClsGlobal.WriteFailTrans(Trans); 

                            }

                            //if (retVal == 1)
                            //{
                            //    Terminal tl = new Terminal();
                            //    tl.PushDatasendSMS(Convert.ToInt64(En.UserID.ToString()), Convert.ToDateTime(En.EntryDate.ToString("yyyy-MM-dd HH:mm:ss")));
                            //}
                            if (!string.IsNullOrEmpty(ErrorMsg.Trim()))
                            {
                                ClsGlobal.WriteErrorLog("OnSave : BioWeb :  " + ErrorMsg.ToString() + " For Device : " + En.TerminalID);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ClsGlobal.WriteErrorLog(" HandleRealtime : BioWeb : " + ex.Message);
            }
        }
        #endregion

        #region "Button Events"

        private void cmdOpenNetWork_Click(object sender, EventArgs e)
        {
            try
            {
                //Get ServerPort Details
                //DataSet ds;
                //DataTable ds;
                //ClsBALTblTransaction ObjClsBALTblTransaction = new ClsBALTblTransaction();

                //ds = ObjClsBALTblTransaction.GetServerPort();

                //if (ds != null &&  ds.Rows.Count > 0 )
                //{
                //    ClsGlobal.ServerPort_BioTime6 = Convert.ToInt32(ds.Rows[0]["ServerPort"]);
                //    ClsGlobal.ServerPort_BioT5 = Convert.ToInt32(ds.Rows[1]["ServerPort"]);
                //    ClsGlobal.ServerPort_BioFace = Convert.ToInt32(ds.Rows[2]["ServerPort"]);
                //    ClsGlobal.ServerPort_BioWeb = Convert.ToInt32(ds.Rows[3]["ServerPort"]);
                //}
                //else
                //{
                //    return;
                //}


                //ClsGlobal.ServerPort_BioTime6 = 7005;
                ClsGlobal.ServerPort_BioT5 = 7005;
                //ClsGlobal.ServerPort_BioFace = 5005;
                //ClsGlobal.ServerPort_BioWeb = 2000;
                //ClsGlobal.ServerPort_BioTime6 = 7004;
                //ClsGlobal.ServerPort_BioT5 = 7006;
                ClsGlobal.ServerPort_BioFace = 1018;
                //ClsGlobal.ServerPort_BioWeb = 7007;
                

                //BioFace
                if (ClsGlobal.ServerPort_BioFace > 0)
                {
                    ClsGlobal.WriteErrorLog("cmdOpenNetWork_Click : BioFace : " + ClsGlobal.ServerPort_BioFace.ToString());
                    try
                    {
                        StartWatchEventThread(); // Start thread.
                    }
                    catch (Exception ex)
                    {
                        ClsGlobal.WriteErrorLog("cmdOpenNetWork_Click : BioFace : " + ex.Message);
                    }
                }


                //BioT5
                //if (ClsGlobal.ServerPort_BioT5 > 0)
                //{
                //    try
                //    {
                //        int vnNetPort = 0;
                //        int vnResultCode = 0;

                //        cmdOpenNetWork.Enabled = false;
                //        vnNetPort = Convert.ToInt32(ClsGlobal.ServerPort_BioT5);
                //        vnResultCode = AxFPCLOCK_Svr1.OpenNetwork(vnNetPort);

                //        if (vnResultCode == ClsGlobal.RUN_SUCCESS)
                //        {
                //            ClsGlobal.WriteEventLog("BioT5 : Host Port : " + vnNetPort + " is Opened Successfully.");
                //            //fbOpenFlag = true;
                //            //FuncEnabledCommand(false);
                //        }
                //        else
                //        {
                //            ClsGlobal.WriteEventLog("BioT5 : Host Port : " + vnNetPort + " is Opened Failed.");
                //            //cmdOpenNetWork.Enabled = true;
                //        }
                //    }
                //    catch (Exception ex)
                //    {
                //        ClsGlobal.WriteErrorLog("cmdOpenNetWork_Click : BioT5 : " + ex.Message);
                //    }
                //}


            }
            catch (Exception ex)
            {
                ClsGlobal.WriteErrorLog("cmdOpenNetWork_Click : " + ex.Message);
            }
            finally
            {
                fbOpenFlag = true;
                FuncEnabledCommand(false);
            }

        }
        private void cmdCloseNetWork_Click(object sender, EventArgs e)
        {
            try
            {
                //Bio T5
                try
                {

                    int vnNetPort = 0;

                    vnNetPort = Convert.ToInt32(ClsGlobal.ServerPort_BioT5);
                    if (fbOpenFlag == true)
                    {
                        //AxFPCLOCK_Svr1.CloseNetwork(vnNetPort);
                        //fbOpenFlag = false;
                        //FuncEnabledCommand(true);
                    }
                }
                catch (Exception ex)
                {
                    ClsGlobal.WriteErrorLog("cmdCloseNetWork_Click : Bio T5 : " + ex.Message);
                }


                //BioFace
                try
                {
                    StopWatchEventThread();	// Stop thread.
                }
                catch (Exception ex)
                {
                    ClsGlobal.WriteErrorLog("cmdCloseNetWork_Click : BioFace : " + ex.Message);
                }

            }
            catch (Exception ex)
            {
                ClsGlobal.WriteErrorLog("cmdCloseNetWork_Click : " + ex.Message);
            }
            finally
            {
                fbOpenFlag = false;
                FuncEnabledCommand(true);
            }
        }

        #endregion

        #region "StripMenuItem"

        private void ShowtoolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                this.ShowInTaskbar = true;
                this.Activate();
                this.WindowState = FormWindowState.Normal;
                this.Visible = true;
            }
            catch (Exception)
            {
            }
        }
        private void HidetoolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                this.ShowInTaskbar = false;
                this.Hide();
            }
            catch (Exception)
            {
            }
        }
        private void ExittoolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                this.ShowInTaskbar = true;
                this.Activate();
                this.WindowState = FormWindowState.Normal;
                this.Visible = true;
                this.Refresh();
                DialogResult result = MessageBox.Show("Are you sure to want to exit applicatoin...?", "PayTime PushTech", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                if (result == DialogResult.Yes)
                {
                    Process.GetCurrentProcess().Kill();
                }
            }
            catch (Exception)
            {
            }
        }
        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            try
            {
                this.ShowInTaskbar = true;
                this.Activate();
                this.WindowState = FormWindowState.Normal;
                this.Visible = true;
            }
            catch (Exception)
            {
            }
        }

        #endregion



    }
}