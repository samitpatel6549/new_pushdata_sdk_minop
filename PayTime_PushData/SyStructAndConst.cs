﻿using System;
using System.Runtime.InteropServices;


namespace SaaSPushTech
{
    enum VerifyKind
    {//
        vrfkDefault = 0x00,			 
        vrfkOnlyFingerPrint = 0x01,			 
        vrfkOnlyPassword = 0x02,			 
        vrfkOnlyCard = 0x03,			 
        vrfkFingerPrint_Or_Password = 0x04,			 
        vrfkFingerPrint_Or_Card = 0x05,			 
        vrfkPassword_Or_Card = 0x06,			 
        vrfkFingerPrint_Or_Card_Or_Password = 0x07,			 
        vrfkFingerPrint_And_Password = 0x08,			 
        vrfkFingerPrint_And_Card = 0x09,			 
        vrfkPassword_And_Card = 0x0A,			 
        vrfkFingerPrint_And_Card_And_Password = 0x0B,			 
    };

    enum UserType
    {// 
        utNormalUser = 0x00,			 
        utAdmin = 0x01,			         
        utRegisterUser = 0x02,			 
        utSuperAdmin = 0x03,			 
        utStoreAdmin = 0x04,			 
    };

    enum LogKind
    {// 
        lkFingerPrint = 0x01,		 
        lkPassword = 0x02,		 
        lkCard = 0x03,		 
        lkFingerPrint_Password = 0x04,		 
        lkFingerPrint_Card = 0x05,		 
        lkPassword_Card = 0x06,		 
        lkFingerPrint_Card_Password = 0x07,		 
    };

    enum SyLastError : int
    {
        sleSuss = 0x0000,	 
        sleSendError = 0x0001,	 
        sleRecvDataError = 0x0002,	 
        sleRecvTimeOut = 0x0003,	 
        sleCallOSFuncError = 0x0004,	 
        sleInvalidParam = 0x0005,	 
        sleNoNewLog = 0x0006,		
        sleDevNotReady = 0x000F,	 

        slePasswordError = 0x0013,	 
        sleInvalidUserID = 0x0030,	 
        sleFullForRegister = 0x0031,	 
        sleFullForUser = 0x0032,	 	
        sleTimeoutForReg = 0x0033,	 
        sleRegistering = 0x0037,	 
        sleNeverRegister = 0x0038,	//Never start registering via PC program
        sleNoRegForBackupID=0x0041, 
        sleNoFunctionInDll = 0x0042, ////dll there is no such function, please contact your supplier 
        sleUnknownError = 0x00FE		 
    }	;

    public struct WorkingShift
    {
        public WorkingSection secA;
        public WorkingSection secB;
        public WorkingSection secC;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct WorkingSection
    {
        public byte startHour;				 
        public byte startMin;				 
        public byte EndHour;				 
        public byte EndMin;					 
        public byte attribute;              
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct SySystemOption
    {
        public byte cLanguage;
        public byte cTimeFmt;				/*Language Formatting*/
        public byte cLockPersonNum;			/*Number combination lock, the 30s effective, Range: 1-5*/

        //daylight saving time
        public byte cDLST_Flag;				/*Daylight saving time has been adjusted, 0:: Daylight Savings Enabled flag is: 1 General*/
        public byte cDLST_mode2_Flag;		/*When daylight saving time*/
        public byte cDLST_En;	            /*Daylight saving time setting is enabled,: = 0: No; non-0: Enable*/
        public byte cDLST_DateFmt;          /*Select Mode 1 and Mode 2, the default is mode 1*/
        public byte cDLST_StartMon;         /*Setting daylight saving time starting month*/
        public byte cDLST_StartDate;        /*Daylight Saving Time begins on the date set, select Mode 1*/
        public byte cDLST_StartWS;          /*Daylight Saving Time began weeks settings, select mode 2*/
        public byte cDLST_StartWK;          /*Daylight Saving Time began weeks settings, select mode 2*/
        public byte cDLST_StartHour;        /*Set at the start of daylight saving time*/
        public byte cDLST_StartMin;         /*Daylight saving time starts the minute setting*/
        public byte cDLST_EndMon;           /*Setting daylight saving time end month*/
        public byte cDLST_EndDate;          /*End date settings, select mode 1 daylight saving time*/
        public byte cDLST_EndWS;            /*End week settings, select mode 2 daylight saving time*/
        public byte cDLST_EndWK;            /*Daylight Saving Time ended the week setting, select mode 2*/
        public byte cDLST_EndHour;          
        public byte cDLST_EndMin;           

        //advanced settings
        public byte cFpShowScore;           /*Whether in the top right corner of the screen display quality value of fingerprint*/
        public byte cFpSecurLev1;           /*Fingerprint 1: 1 safety rating: 0--5, default is 3*/
        public byte cFpSecurLevN;           /*Fingerprint 1: N Security Level: 0--5, default is 3*/
        public byte cUserIdMust;            /*You must enter a user ID,: = 0: No; non-zero: Yes*/
        public byte cTwoSensorsEn;          /*External fingerprint device,: = 0: No; non-zero: Yes*/
        public byte cVoiceHintEn;           /*Voice prompts enabled,: = 0: No; non-zero: Yes*/

        //A variety of card authentication settings
        public byte cNoCardOnly;            /*只验证号码卡（即ID卡）, : =0:否;非0:是*/
        public byte cRegCardOnly;           /*只验证注册过的卡, : =0:否;非0:是*/
        public byte cFpCardOnly;            /*只验证指纹卡（即IC卡）, : =0:否;非0:是*/

        //Additional feature set 
        public byte cRemoteVerify;          /*Remote Authentication, four ways: NL, LN NO, LO*/

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
        public byte[] cAuthServerIP;          /*Set IIS server's IP address*/
        public byte cWorkCode;              /*Working Number Mode: None, Mode 1, Mode 2*/
        public byte cButtonBeepEn;          /*Keyboard tone,: = 0: No; non-zero: Yes*/
        public byte cAdjustVoice;           /*Adjust the voice volume, range: 0 to 100%, default is 34%*/
        public byte cImdtPrintEn;           /*Even printing,: = 0: No; non-zero: Yes*/
        public byte cDefendTailEn;          /*Submarine,: = 0: No; non-zero: Yes*/
        public byte cUserPswEdit;           /*User Password Change Enable: = 0: No; non-zero: Yes*/
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct SyDailyEntryOption
    {
        public UInt16 wAlarmAdminLogFreeNum;	/*Manage Logs remaining capacity alarm, the default is 99*/
        public UInt16 wAlarmLogFreeNum;       /*Attendance remaining capacity alarm logs, the default is 99*/
        public byte cRecheckMin;            /*Repeat confirmation time, unit: minutes. The default is 0 */
        public byte cReadyBlock;            /* Flash preliminary block for the log count, unit 4K. The default is 1 */
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct SyTimeFormat
    {
        public UInt16 wYear;					 
        public byte cMonth;					 
        public byte cDate;					 
        public byte cDay;					 
        public byte cHour;					 
        public byte cMin;					 
        public byte cSec;					 
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct SyCommOption
    {
        public byte cBaudRate;				/*Baud rate settings, multiple 9600, ranging from 1 to 12*/
        public UInt32 cDevNum;				/*Machine number of numbers from 1 to 255 */
        public byte cDHCP_EN;				/*Dynamic Host Configuration enabled: = 0: No; non-zero: Yes*/
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
        public byte[] cIP_Addr;   			/*IP address settings*/
        public byte cNetSpeed;				/*Ethernet speeds, default is AUTO*/
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
        public byte[] cNetMask;	    		/*The default subnet mask of 255.255.255.0*/
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
        public byte[] cGateway;		    	/*Default gateway address 0.0.0.0*/
        public byte cEthernetEn;			/*0: No; non-zero: Yes*/
        public byte cRS232_En;				/*0: No; non-zero: Yes*/
        public byte cRS485_En;				/*0: No; non-zero: Yes*/
        public byte cUSB_En;				/*USB communication is enabled: = 0: No; non-zero: Yes*/
        public byte cUP_Now_Event;			/*Active upload enabled: = 0: no; 1: yes*/
        public UInt32 dwLinkPws;				/*Communication connection password, range 0 to 999999, the default is 0 */
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
        public byte[] cMac;   				/*The default MAC address 0.0.0.0.0.0*/
        public UInt16 wLporta;				/*The default local port 8000 */
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
        public byte[] sSeverIp;   			/*The default server IP address 192.168.1.100*/
        public UInt16 wRport;					/*The default server port number 9000*/
        public byte cWiegand;				/*Wiegand mode selection: = 0:26; 0:34 Non-*/
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct SyPowerTime
    {
        public byte cHour;					 
        public byte cMin;					 
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct SyPowerManage
    {
        public SyPowerTime sShutdown;				/*Time off, the default value is 0, which indicates no*/
        public SyPowerTime sPowerOn;			    /*Boot time, the default value is 0, which indicates no*/
        public SyPowerTime sSleep;					/*Regular sleep, the default value is 0, which indicates no*/
        public byte cIdleMode;				/*0 = no, 1 = on, 2 = off, 3 = sleep, the default is no*/
        public UInt16 wIdleMin;				/*Idle time: minutes, range: 0 to 999*/

        //The timing when the bell rings and long
        public byte cBellSec;				/*Bell time, unit: second, range: 0 to 255*/
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
        public SyPowerTime[] sBell;			    	/*8 regular rings periods, invalid: 0xFFFF*/

        // lock off key
        public byte cLockPowButton;         /*锁定关机键，: =0:否;非0:是*/
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct SySystemInfo
    {
        public UInt16 wUserCnt;           /*User registration number*/
        public UInt16 wAdminCnt;          /*Administrator registration number*/
        public UInt16 wTempCnt;           /*Temporary user registration number*/
        public UInt16 wFpCnt;             /*Fingerprint registration number*/
        public UInt16 wPwsCnt;            /*Password Registration Number*/
        public UInt16 wCardCnt;           /*ID card registration number*/
        public UInt32 wAttLogCnt;         /*Attendance Records*/
        public UInt32 wAdminLogCnt;       /*Management records*/
        public UInt32 wLogCnt;            /*Number of log records*/

        //The remaining capacity
        public UInt16 wFpFreeNum;         /*Fingerprint Enrollment margin*/
        public UInt16 wPwsFreeNum;        /*Password Registration margin*/
        public UInt16 wCardFreeNum;       /*ID card registration margin*/
        public UInt32 wLogFreeNum;        /*Record margin*/

        //Device Information
        public UInt16 wFpNum;				/*Fingerprint capacity*/
        public UInt32 wLogNum;            /*Recording capacity, unit: ten thousand*/
        public SyTimeFormat sManuTime;          /*Date of manufacture*/
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 20)]
        public byte[] cSerialNum;         /*Manufacturer Serial Number*/
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 24)]
        public byte[] cManuName;          /*Manufacturers*/
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 24)]
        public byte[] cDevName;           /*Device Name*/
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
        public byte[] cAlgVer;            /*Algorithm version*/
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 24)]
        public byte[] cFirmwareVer;       /*Firmware version*/
        public UInt32 wCodingPSW;			/*Programming password*/
        public UInt16 wCommonPSW;		    /*Universal Password*/
        public UInt16 wEquipmentNum;      /*device ID*/
        public byte wPowerOffKey;       /*Off key enabled*/
        public UInt32 wNewRcdOffSet;      /*The new recording starting position*/
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct SyFingerTemplate
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 512)]
        public byte[] data;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct SyTimeSeg
    {
        public byte startHour;				
        public byte startMin;				
        public byte EndHour;				
        public byte EndMin;					
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct SyWeekTime
    {
        public SyTimeSeg sSun;					
        public SyTimeSeg sMon;					
        public SyTimeSeg sTues;					
        public SyTimeSeg sWed;					
        public SyTimeSeg sThurs;				
        public SyTimeSeg sFrid;					
        public SyTimeSeg sSat;					
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct SyAccessOption
    {
        public byte cLockTime;				/*Unlock Time: Unit: 20ms; the range: 0-5.08s*/
        public byte cDSenDelay;	            /*Magnetic Delay: Unit: 1s; the range: 0-99s */
        public byte cDSenMode;				/*Magnetic switch include three types: None, normally open, normally closed*/

        //Alarm Settings
        public byte cAlarmHelpKey;			/*Help button choice: = 0: No; non-zero: Yes*/
        public byte cAlarmFpTrig1;			/*1: 1 ratio for alarm: = 0: No; non-zero: Yes*/
        public byte cAlarmFpTrigN;			/*1: N ratio of the alarm: = 0: No; non-zero: Yes*/
        public byte cAlarmPwsTrig;			/*Password ratio of alarm: = 0: No; non-zero: Yes*/
        public byte cAlarmDelay;             /*Alarm Delay: Unit: 1s; 0-255s*/
        public byte cAlarmFailCnt;		    /*Wrong case to the police; Range: 0-9 */
        public byte WorkingFlag;		    	/*Non-time surveillance to enable flag*/
        public SyTimeSeg WorkingTime;			/*Non-dispatched time, any person other than a time not unlock*/
        public byte cAlarmTamper;
        public byte cCheckDoorState;
        public byte cAlarmIllegalDoor;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct SyGroupRestrict
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 3)]
        public byte[] cValidTime; 			/*Group validity period, the effective period of time when the set is turned on, the user can replace the effective period of time, there are 50 periods, optionally three groups*/
        public byte cVerifyType;			/*Group authentication type, authentication type when the group turned alternative user authentication type, there are currently 14 groups Authentication Type*/
    }

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi, Pack = 1)]
    public struct SyUserInfo
    {
        public UInt16 wUserID;			/*User ID, decimal store*/
        public Byte sUserType;			/*User type: Normal, registrar, administrator, super administrator, provisional, anti-stress*/
        public Byte sEnrollStatus;		/*The index table user status*/
        public UInt32 dwHoldTime;			/*Casual users effective time, with respect to at 0:00 on January 1, 2010 秒*/
        public byte cGroup;				/*Grouping, currently five groups*/
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 3)]
        public byte[] cValidTime;		    /*Users validity period, the effective period of time when the group is on, users can replace the effective period of time, there are 50 periods, optionally three groups*/
        public byte cVerifyType;		/*User authentication, authentication types when the group turned alternative user authentication type, there are currently 14 groups Authentication Type*/
        public byte cAccessComb;		/*Unlock combination, there are currently 10 unlock combinations*/
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 16)]
        public string name;  			/*User name, supports eight characters GB code*/
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 5)]
        public UInt16[] MbIndex;			/*The offset is used to store templates*/
    }

    [StructLayoutAttribute(LayoutKind.Sequential, Pack = 1)]
    public struct SyUserInfoExt
    {
        public SyUserInfo sUserInfo;
        public int dwPws;	 
        public int dwCardID;  
    }

    [StructLayoutAttribute(LayoutKind.Sequential, Pack = 1)]
    public struct SyLogRecord //LOG_RECORD
    {
        public UInt16 wUserID;			        /* */
        public byte cLogState;			            /* */
        public Byte cLogType;			/* */
        public UInt32 sRecordTime;		/*Log Time with respect to 2000*/
    }

    public struct SyDate
    {
        public int year;
        public int month;
        public int day;
        public SyDate(int year, int month, int day)
        {
            this.year = year;
            this.month = month;
            this.day = day;
        }
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct SyComCfg
    {
        public byte mPortNo;
        public UInt32 mBaudRate;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct SyNetCfg
    {
        public UInt16 mPortNo;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
        public byte[] mIPAddr;
        public byte mIsTCP;
    }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct BELL_CELL
    { 
	    public byte Hour;  			  
	    public byte Min;   			 
	    public byte Sec;				 
	    public byte enable;  			  
    };        //1562 BYTE

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public  struct SYS_BELL
    {
        public byte BellTime;			//Alarm number - Time s
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 30)]
        public BELL_CELL[] XinQi;	//Alarm 30
    };      //1562 BYTE
}

