﻿
using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
namespace SaaSPushTech
{
	public class ClsEncryptionMain
	{

		public string EncryptPassword(string Password)
		{

			//Encrypt the Password
			string sEncryptedPassword = "";
			string sEncryptKey = "P@SSW@RD@09";
			//Should be minimum 8 characters

			try {
				sEncryptedPassword = ClsEncryptionRoot.EncryptPasswordMD5(Password, sEncryptKey);

			} catch (Exception ex) {
				return sEncryptedPassword;
			}

			return sEncryptedPassword;
		}

		public string DecryptPassword(string Password)
		{
			//Encrypt the Password
			string sDecryptedPassword = "";
			string sEncryptKey = "P@SSW@RD@09";
			//Should be minimum 8 characters

			try {
				sDecryptedPassword = ClsEncryptionRoot.DecryptPasswordMD5(Password, sEncryptKey);

			} catch (Exception ex) {
				return sDecryptedPassword;
			}

			return sDecryptedPassword;
		}
	}
}
