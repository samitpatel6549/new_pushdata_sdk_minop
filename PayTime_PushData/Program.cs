using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Security.Permissions;
using System.Net.Mail;
using System.Net;
namespace SaaSPushTech
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        [SecurityPermission(SecurityAction.Demand, Flags = SecurityPermissionFlag.ControlAppDomain)]
        static void Main()
        {

            AppDomain currentDomain = AppDomain.CurrentDomain;
            currentDomain.UnhandledException += new UnhandledExceptionEventHandler(MyHandler);
            

            try
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new FrmLog());
                
            }
            catch (Exception ex)
            {
                ClsGlobal.WriteErrorLog("OnProgramMain : " + ex.Message);
            }
        }

        static void MyHandler(object sender, UnhandledExceptionEventArgs args)
        {
            try
            {
                
                Exception e = (Exception)args.ExceptionObject;
                MailMessage msg = new MailMessage();
                msg.From = new MailAddress("jwalit.katira@mantratec.com");
                msg.To.Add("samit.patel@mantratec.com");
                msg.CC.Add("balvant.katariya@mantratec.com");
                msg.Bcc.Add("jwalit.katira@mantratec.com");
                msg.Subject = "Exception in Minoppushdata";
                msg.Body = "MyHandler caught : " + e.Message +"Method:" +e.StackTrace+"";
                SmtpClient smt = new SmtpClient();
                smt.Host = "smtp.rediffmailpro.com";
                System.Net.NetworkCredential ntcd = new NetworkCredential();
                ntcd.UserName = "jwalit.katira@mantratec.com";
                ntcd.Password = "jwalit";
                smt.Credentials = ntcd;
                //smt.EnableSsl = true;
                //smt.UseDefaultCredentials = true;
                smt.DeliveryMethod = SmtpDeliveryMethod.Network;
                smt.Port = 587;
                smt.Send(msg);
            }
            catch (Exception ex)
            {
                ClsGlobal.WriteErrorLog("On Minoppushdata Exception MailSend : " + ex.Message );
            }
        }
    }
}