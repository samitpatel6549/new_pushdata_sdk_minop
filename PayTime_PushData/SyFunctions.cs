﻿using System;
using System.Runtime.InteropServices;

namespace SaaSPushTech
{
    using SyUserType = Byte;
    using SyDailyEntryTime = UInt32;
    //函数导入声明
    class SyFunctions
    {
        [DllImport("xfic2.dll",
            CharSet = CharSet.Unicode,
            CallingConvention = CallingConvention.StdCall,
            BestFitMapping = true,
            ExactSpelling = false,
            SetLastError = false,
            ThrowOnUnmappableChar = false)]
        public static extern int CmdOpenDevice(Byte dk, IntPtr param, UInt32 devAddr, UInt32 password, Byte traceIO);

        [DllImport("xfic2.dll",
            CharSet = CharSet.Unicode,
            CallingConvention = CallingConvention.StdCall,
            BestFitMapping = true,
            ExactSpelling = false,
            SetLastError = false,
            ThrowOnUnmappableChar = false)]
        public static extern int CloseCom();  //CmdCloseDevice


        [DllImport("xfic2.dll",
            CharSet = CharSet.Unicode,
            CallingConvention = CallingConvention.StdCall,
            BestFitMapping = true,
            ExactSpelling = false,
            SetLastError = false,
            ThrowOnUnmappableChar = false)]
        public static extern int CmdHardCloseDevice();

        [DllImport("xfic2.dll",
            CharSet = CharSet.Unicode,
            CallingConvention = CallingConvention.StdCall,
            BestFitMapping = true,
            ExactSpelling = false,
            SetLastError = false,
            ThrowOnUnmappableChar = false)]
        public static extern int CmdTestConn2Device();

        [DllImport("xfic2.dll",
            CharSet = CharSet.Unicode,
            CallingConvention = CallingConvention.StdCall,
            BestFitMapping = true,
            ExactSpelling = false,
            SetLastError = false,
            ThrowOnUnmappableChar = false)]
        public static extern int CmdLockDevice();

        [DllImport("xfic2.dll",
            CharSet = CharSet.Unicode,
            CallingConvention = CallingConvention.StdCall,
            BestFitMapping = true,
            ExactSpelling = false,
            SetLastError = false,
            ThrowOnUnmappableChar = false)]
        public static extern  int CmdUnLockDevice();

        [DllImport("xfic2.dll",
            CharSet = CharSet.Unicode,
            CallingConvention = CallingConvention.StdCall,
            BestFitMapping = true,
            ExactSpelling = false,
            SetLastError = false,
            ThrowOnUnmappableChar = false)]
        public static extern  int CmdGotoISP();

        [DllImport("xfic2.dll",
            CharSet = CharSet.Unicode,
            CallingConvention = CallingConvention.StdCall,
            BestFitMapping = true,
            ExactSpelling = false,
            SetLastError = false,
            ThrowOnUnmappableChar = false)]
        public static extern  int CmdOpenDoor();

        [DllImport("xfic2.dll",
            CharSet = CharSet.Unicode,
            CallingConvention = CallingConvention.StdCall,
            BestFitMapping = true,
            ExactSpelling = false,
            SetLastError = false,
            ThrowOnUnmappableChar = false)]
        public static extern  int CmdCloseDoor();

        [DllImport("xfic2.dll",
            CharSet = CharSet.Unicode,
            CallingConvention = CallingConvention.StdCall,
            BestFitMapping = true,
            ExactSpelling = false,
            SetLastError = false,
            ThrowOnUnmappableChar = false)]
        public static extern  int CmdSet2Default();

        [DllImport("xfic2.dll",
            CharSet = CharSet.Unicode,
            CallingConvention = CallingConvention.StdCall,
            BestFitMapping = true,
            ExactSpelling = false,
            SetLastError = false,
            ThrowOnUnmappableChar = false)]
        public static extern  int CmdDeviceBeep();

        [DllImport("xfic2.dll",
            CharSet = CharSet.Unicode,
            CallingConvention = CallingConvention.StdCall,
            BestFitMapping = true,
            ExactSpelling = false,
            SetLastError = false,
            ThrowOnUnmappableChar = false)]
        public static extern  int CmdRebootDevice();

        [DllImport("xfic2.dll",
            CharSet = CharSet.Unicode,
            CallingConvention = CallingConvention.StdCall,
            BestFitMapping = true,
            ExactSpelling = false,
            SetLastError = false,
            ThrowOnUnmappableChar = false)]
        public static extern  int CmdClearAdmin();

        [DllImport("xfic2.dll",
            CharSet = CharSet.Unicode,
            CallingConvention = CallingConvention.StdCall,
            BestFitMapping = true,
            ExactSpelling = false,
            SetLastError = false,
            ThrowOnUnmappableChar = false)]
        public static extern  int CmdClearAllUser();

        [DllImport("xfic2.dll",
            CharSet = CharSet.Unicode,
            CallingConvention = CallingConvention.StdCall,
            BestFitMapping = true,
            ExactSpelling = false,
            SetLastError = false,
            ThrowOnUnmappableChar = false)]
        public static extern  int CmdClearAllTempUser();


        [DllImport("xfic2.dll",
            CharSet = CharSet.Unicode,
            CallingConvention = CallingConvention.StdCall,
            BestFitMapping = true,
            ExactSpelling = false,
            SetLastError = false,
            ThrowOnUnmappableChar = false)]
        public static extern  int CmdClearAllCoerceFingerPrint();

        [DllImport("xfic2.dll",
            CharSet = CharSet.Unicode,
            CallingConvention = CallingConvention.StdCall,
            BestFitMapping = true,
            ExactSpelling = false,
            SetLastError = false,
            ThrowOnUnmappableChar = false)]
        public static extern  int CmdClearAllDailyEntry();

        [DllImport("xfic2.dll",
            CharSet = CharSet.Unicode,
            CallingConvention = CallingConvention.StdCall,
            BestFitMapping = true,
            ExactSpelling = false,
            SetLastError = false,
            ThrowOnUnmappableChar = false)]
        public static extern  int CmdGetTime(ref UInt16 Year,
        ref Byte Month,
        ref Byte Date,
        //ref Byte Day,
        ref Byte Hour,
        ref Byte Min,
        ref Byte Sec
        );

        [DllImport("xfic2.dll",
            CharSet = CharSet.Unicode,
            CallingConvention = CallingConvention.StdCall,
            BestFitMapping = true,
            ExactSpelling = false,
            SetLastError = false,
            ThrowOnUnmappableChar = false)]
        public static extern  int CmdSetDeviceTime(UInt16 Year,
                                                            Byte Month,
                                                            Byte Date,
                                                            Byte Day,
                                                            Byte Hour,
                                                            Byte Min,
                                                            Byte Sec
                                                            );

        [DllImport("xfic2.dll",
            CharSet = CharSet.Unicode,
            CallingConvention = CallingConvention.StdCall,
            BestFitMapping = true,
            ExactSpelling = false,
            SetLastError = false,
            ThrowOnUnmappableChar = false)]
        public static extern  int CmdGetCorrectImage(Char[] fileName);


        [DllImport("xfic2.dll",
            CharSet = CharSet.Unicode,
            CallingConvention = CallingConvention.StdCall,
            BestFitMapping = true,
            ExactSpelling = false,
            SetLastError = false,
            ThrowOnUnmappableChar = false)]
        public static extern  int CmdGetCommOption(ref SyCommOption co);

        [DllImport("xfic2.dll",
            CharSet = CharSet.Unicode,
            CallingConvention = CallingConvention.StdCall,
            BestFitMapping = true,
            ExactSpelling = false,
            SetLastError = false,
            ThrowOnUnmappableChar = false)]
        public static extern  int CmdSetCommOption(ref SyCommOption co);

        [DllImport("xfic2.dll",
            CharSet = CharSet.Unicode,
            CallingConvention = CallingConvention.StdCall,
            BestFitMapping = true,
            ExactSpelling = false,
            SetLastError = false,
            ThrowOnUnmappableChar = false)]
        public static extern  int CmdGetDailyEntryOption(ref SyDailyEntryOption deo);

        [DllImport("xfic2.dll",
            CharSet = CharSet.Unicode,
            CallingConvention = CallingConvention.StdCall,
            BestFitMapping = true,
            ExactSpelling = false,
            SetLastError = false,
            ThrowOnUnmappableChar = false)]
        public static extern  int CmdSetDailyEntryOption(ref SyDailyEntryOption deo);

        [DllImport("xfic2.dll",
            CharSet = CharSet.Unicode,
            CallingConvention = CallingConvention.StdCall,
            BestFitMapping = true,
            ExactSpelling = false,
            SetLastError = false,
            ThrowOnUnmappableChar = false)]
        public static extern  int CmdGetSystemOption(ref SySystemOption so);

        [DllImport("xfic2.dll",
            CharSet = CharSet.Unicode,
            CallingConvention = CallingConvention.StdCall,
            BestFitMapping = true,
            ExactSpelling = false,
            SetLastError = false,
            ThrowOnUnmappableChar = false)]
        public static extern  int CmdSetSystemOption(ref SySystemOption so);

        [DllImport("xfic2.dll",
            CharSet = CharSet.Unicode,
            CallingConvention = CallingConvention.StdCall,
            BestFitMapping = true,
            ExactSpelling = false,
            SetLastError = false,
            ThrowOnUnmappableChar = false)]
        public static extern  int CmdGetPowerManage(ref SyPowerManage pm);

        [DllImport("xfic2.dll",
            CharSet = CharSet.Unicode,
            CallingConvention = CallingConvention.StdCall,
            BestFitMapping = true,
            ExactSpelling = false,
            SetLastError = false,
            ThrowOnUnmappableChar = false)]
        public static extern  int CmdSetPowerManage(ref SyPowerManage pm);

        [DllImport("xfic2.dll",
            CharSet = CharSet.Unicode,
            CallingConvention = CallingConvention.StdCall,
            BestFitMapping = true,
            ExactSpelling = false,
            SetLastError = false,
            ThrowOnUnmappableChar = false)]
        public static extern  int CmdGetSystemInfo(ref SySystemInfo si);

        [DllImport("xfic2.dll",
            CharSet = CharSet.Unicode,
            CallingConvention = CallingConvention.StdCall,
            BestFitMapping = true,
            ExactSpelling = false,
            SetLastError = false,
            ThrowOnUnmappableChar = false)]
        public static extern  int CmdSetSystemInfo(ref SySystemInfo si);

        [DllImport("xfic2.dll",
            CharSet = CharSet.Unicode,
            CallingConvention = CallingConvention.StdCall,
            BestFitMapping = true,
            ExactSpelling = false,
            SetLastError = false,
            ThrowOnUnmappableChar = false)]
        public static extern  int CmdGetAccessOption(ref SyAccessOption ao);

        [DllImport("xfic2.dll",
            CharSet = CharSet.Unicode,
            CallingConvention = CallingConvention.StdCall,
            BestFitMapping = true,
            ExactSpelling = false,
            SetLastError = false,
            ThrowOnUnmappableChar = false)]
        public static extern  int CmdSetAccessOption(ref SyAccessOption ao);


        [DllImport("xfic2.dll",
            CharSet = CharSet.Unicode,
            CallingConvention = CallingConvention.StdCall,
            BestFitMapping = true,
            ExactSpelling = false,
            SetLastError = false,
            ThrowOnUnmappableChar = false)]
        public static extern  int CmdGetGroup(ref SyGroupRestrict gr, byte grId);

        [DllImport("xfic2.dll",
            CharSet = CharSet.Unicode,
            CallingConvention = CallingConvention.StdCall,
            BestFitMapping = true,
            ExactSpelling = false,
            SetLastError = false,
            ThrowOnUnmappableChar = false)]
        public static extern  int CmdSetGroup(ref SyGroupRestrict gr, byte grId);

        [DllImport("xfic2.dll",
            CharSet = CharSet.Unicode,
            CallingConvention = CallingConvention.StdCall,
            BestFitMapping = true,
            ExactSpelling = false,
            SetLastError = false,
            ThrowOnUnmappableChar = false)]
        public static extern  int CmdGetCooperationTeam(ref UInt16 ct, byte ctId);

        [DllImport("xfic2.dll",
            CharSet = CharSet.Unicode,
            CallingConvention = CallingConvention.StdCall,
            BestFitMapping = true,
            ExactSpelling = false,
            SetLastError = false,
            ThrowOnUnmappableChar = false)]
        public static extern  int CmdSetCooperationTeam(UInt16 ct, byte ctId);

        [DllImport("xfic2.dll",
            CharSet = CharSet.Unicode,
            CallingConvention = CallingConvention.StdCall,
            BestFitMapping = true,
            ExactSpelling = false,
            SetLastError = false,
            ThrowOnUnmappableChar = false)]
        public static extern  int CmdGetAccessTimeParam(ref SyWeekTime wt, byte wtId);

        [DllImport("xfic2.dll",
            CharSet = CharSet.Unicode,
            CallingConvention = CallingConvention.StdCall,
            BestFitMapping = true,
            ExactSpelling = false,
            SetLastError = false,
            ThrowOnUnmappableChar = false)]
        public static extern  int CmdSetAccessTimeParam(ref SyWeekTime wt, byte wtId);

        [DllImport("xfic2.dll",
            CharSet = CharSet.Unicode,
            CallingConvention = CallingConvention.StdCall,
            BestFitMapping = true,
            ExactSpelling = false,
            SetLastError = false,
            ThrowOnUnmappableChar = false)]
        public static extern  int CmdGetCountOfValidUserID(ref UInt16 cnt);

        [DllImport("xfic2.dll",
            CharSet = CharSet.Unicode,
            CallingConvention = CallingConvention.StdCall,
            BestFitMapping = true,
            ExactSpelling = false,
            SetLastError = false,
            ThrowOnUnmappableChar = false)]
        public static extern  int CmdGetCount(ref UInt16 cnt);

        [DllImport("xfic2.dll",
            CharSet = CharSet.Unicode,
            CallingConvention = CallingConvention.StdCall,
            BestFitMapping = true,
            ExactSpelling = false,
            SetLastError = false,
            ThrowOnUnmappableChar = false)]
        public static extern  int CmdGetAllValidUserID([In, Out] UInt16[] idIndex, UInt16 cnt);

        [DllImport("xfic2.dll",
            CharSet = CharSet.Unicode,
            CallingConvention = CallingConvention.StdCall,
            BestFitMapping = true,
            ExactSpelling = false,
            SetLastError = false,
            ThrowOnUnmappableChar = false)]
        public static extern  int CmdGetFreeUserID(ref UInt16 userId, byte kind);

        [DllImport("xfic2.dll",
            CharSet = CharSet.Unicode,
            CallingConvention = CallingConvention.StdCall,
            BestFitMapping = true,
            ExactSpelling = false,
            SetLastError = false,
            ThrowOnUnmappableChar = false)]
        public static extern  int CmdDeleteUserInfo(UInt16 userId, byte kind);

        [DllImport("xfic2.dll",
            CharSet = CharSet.Unicode,
            CallingConvention = CallingConvention.StdCall,
            BestFitMapping = true,
            ExactSpelling = false,
            SetLastError = false,
            ThrowOnUnmappableChar = false)]
        public static extern  int CmdTemp2NormalUser(UInt16 userId);

        [DllImport("xfic2.dll",
            CharSet = CharSet.Unicode,
            CallingConvention = CallingConvention.StdCall,
            BestFitMapping = true,
            ExactSpelling = false,
            SetLastError = false,
            ThrowOnUnmappableChar = false)]
        public static extern  int CmdDeleteCoerceFingerPrintByUserID(UInt16 userId);

        [DllImport("xfic2.dll",
            CharSet = CharSet.Unicode,
            CallingConvention = CallingConvention.StdCall,
            BestFitMapping = true,
            ExactSpelling = false,
            SetLastError = false,
            ThrowOnUnmappableChar = false)]
        public static extern  int CmdGetUserInfo(ref SyUserInfoExt uie, UInt16 userId);

        [DllImport("xfic2.dll",
            CharSet = CharSet.Unicode,
            CallingConvention = CallingConvention.StdCall,
            BestFitMapping = true,
            ExactSpelling = false,
            SetLastError = false,
            ThrowOnUnmappableChar = false)]
        public static extern  int CmdGetUserInfo2(ref SyUserInfoExt uie, UInt16 userId);

        [DllImport("xfic2.dll",
            CharSet = CharSet.Unicode,
            CallingConvention = CallingConvention.StdCall,
            BestFitMapping = true,
            ExactSpelling = false,
            SetLastError = false,
            ThrowOnUnmappableChar = false)]
        public static extern  int CmdSetUserInfo(ref SyUserInfoExt uie, UInt16 userId);

        [DllImport("xfic2.dll",
            CharSet = CharSet.Unicode,
            CallingConvention = CallingConvention.StdCall,
            BestFitMapping = true,
            ExactSpelling = false,
            SetLastError = false,
            ThrowOnUnmappableChar = false)]
        public static extern  int CmdRegisterUser(ref SyUserInfoExt uie, byte kind);

        [DllImport("xfic2.dll",
            CharSet = CharSet.Unicode,
            CallingConvention = CallingConvention.StdCall,
            BestFitMapping = true,
            ExactSpelling = false,
            SetLastError = false,
            ThrowOnUnmappableChar = false)]
        public static extern  int CmdReg(ref SyUserInfoExt uie, byte kind);

        [DllImport("xfic2.dll",
            CharSet = CharSet.Unicode,
            CallingConvention = CallingConvention.StdCall,
            BestFitMapping = true,
            ExactSpelling = false,
            SetLastError = false,
            ThrowOnUnmappableChar = false)]
        public static extern  int CmdGetLastRegisterResult();


        [DllImport("xfic2.dll",
            CharSet = CharSet.Unicode,
            CallingConvention = CallingConvention.StdCall,
            BestFitMapping = true,
            ExactSpelling = false,
            SetLastError = false,
            ThrowOnUnmappableChar = false)]
        public static extern  int CmdGetCountOfUserFingerPrint(ref UInt16 cnt, UInt16 userId);

        [DllImport("xfic2.dll",
            CharSet = CharSet.Unicode,
            CallingConvention = CallingConvention.StdCall,
            BestFitMapping = true,
            ExactSpelling = false,
            SetLastError = false,
            ThrowOnUnmappableChar = false)]
        public static extern  int CmdGetUserFingerPrint(ref SyFingerTemplate ft, UInt16 userId, UInt16 ftId);

        [DllImport("xfic2.dll",
            CharSet = CharSet.Unicode,
            CallingConvention = CallingConvention.StdCall,
            BestFitMapping = true,
            ExactSpelling = false,
            SetLastError = false,
            ThrowOnUnmappableChar = false)]
        public static extern  int CmdSetCountOfUserFingerPrint(UInt16 userId, UInt16 ftCnt);

        [DllImport("xfic2.dll",
            CharSet = CharSet.Unicode,
            CallingConvention = CallingConvention.StdCall,
            BestFitMapping = true,
            ExactSpelling = false,
            SetLastError = false,
            ThrowOnUnmappableChar = false)]
        public static extern  int CmdSetUserFingerPrint(ref SyFingerTemplate ft, UInt16 userId, UInt16 ftId);


        [DllImport("xfic2.dll",
            CharSet = CharSet.Unicode,
            CallingConvention = CallingConvention.StdCall,
            BestFitMapping = true,
            ExactSpelling = false,
            SetLastError = false,
            ThrowOnUnmappableChar = false)]
        public static extern  int CmdGetCountOfValidDailyEntryRecord(ref int totalCnt, ref int unreadPos);

        [DllImport("xfic2.dll",
            CharSet = CharSet.Unicode,
            CallingConvention = CallingConvention.StdCall,
            BestFitMapping = true,
            ExactSpelling = false,
            SetLastError = false,
            ThrowOnUnmappableChar = false)]
        public static extern  int CmdGetValidDailyEntryRecord([In, Out] SyLogRecord[] der, int location, byte cnt);

        [DllImport("xfic2.dll",
            CharSet = CharSet.Unicode,
            CallingConvention = CallingConvention.StdCall,
            BestFitMapping = true,
            ExactSpelling = false,
            SetLastError = false,
            ThrowOnUnmappableChar = false)]
        public static extern  int CmdStressTest(ref SyLogRecord der, UInt16 cnt);

        [DllImport("xfic2.dll",
            CharSet = CharSet.Unicode,
            CallingConvention = CallingConvention.StdCall,
            BestFitMapping = true,
            ExactSpelling = false,
            SetLastError = false,
            ThrowOnUnmappableChar = false)]
        public static extern  int CmdStartDetectionNewDailyEntryRecord(ref SyNetCfg nc);

        [DllImport("xfic2.dll",
            CharSet = CharSet.Unicode,
            CallingConvention = CallingConvention.StdCall,
            BestFitMapping = true,
            ExactSpelling = false,
            SetLastError = false,
            ThrowOnUnmappableChar = false)]
        public static extern  void CmdEndDetectionNewDailyEntryRecord();

        [DllImport("xfic2.dll",
            CharSet = CharSet.Unicode,
            CallingConvention = CallingConvention.StdCall,
            BestFitMapping = true,
            ExactSpelling = false,
            SetLastError = false,
            ThrowOnUnmappableChar = false)]
        public static extern  int CmdExtractLastDailyEntry(ref SyLogRecord der, ref UInt32 devId);

        [DllImport("xfic2.dll",
            CharSet = CharSet.Unicode,
            CallingConvention = CallingConvention.StdCall,
            BestFitMapping = true,
            ExactSpelling = false,
            SetLastError = false,
            ThrowOnUnmappableChar = false)]
        public static extern  void DecodeSyUserType(Byte ut,
            ref byte power,
            ref byte isTemp,
            ref byte hasCPF);

        [DllImport("xfic2.dll",
            CharSet = CharSet.Unicode,
            CallingConvention = CallingConvention.StdCall,
            BestFitMapping = true,
            ExactSpelling = false,
            SetLastError = false,
            ThrowOnUnmappableChar = false)]
        public static extern  void EncodeSyUserType(ref Byte ut,
            byte power,
            byte isTemp,
            byte hasCPF);

        [DllImport("xfic2.dll",
            CharSet = CharSet.Unicode,
            CallingConvention = CallingConvention.StdCall,
            BestFitMapping = true,
            ExactSpelling = false,
            SetLastError = false,
            ThrowOnUnmappableChar = false)]
        public static extern  void DecodeSyEnrollState(Byte es,
            ref byte fpCnt,
            ref byte enablePswd,
            ref byte enableCard,
            ref byte enable);

        [DllImport("xfic2.dll",
            BestFitMapping = true,
            ExactSpelling = false,
            SetLastError = false,
            ThrowOnUnmappableChar = false)]
        public static extern  void EncodeSyEnrollState(ref Byte es,
            byte fpCnt,
            byte enablePswd,
            byte enableCard,
            byte enable);

        [DllImport("xfic2.dll")]
        public static extern  void DecodeSyDailyEntryType(Byte det,
            ref byte kind,
            ref byte hasCooperated,
            ref byte hasSetup,
            ref byte hasOpenDoor,
            ref byte hasAlarm);

        [DllImport("xfic2.dll",
            CharSet = CharSet.Unicode,
            CallingConvention = CallingConvention.StdCall,
            BestFitMapping = true,
            ExactSpelling = false,
            SetLastError = false,
            ThrowOnUnmappableChar = false)]
        public static extern  void DecodeSyDailyEntryTime(UInt32 det,
            ref byte sec,
            ref byte min,
            ref byte hour,
            ref byte date,
            ref byte month,
            ref UInt16 year);


        [DllImport("xfic2.dll",
            CharSet = CharSet.Unicode,
            CallingConvention = CallingConvention.StdCall,
            BestFitMapping = true,
            ExactSpelling = false,
            SetLastError = false,
            ThrowOnUnmappableChar = false)]
        public static extern  UInt32 EncodeUserHoldTime(
             byte sec,
             byte min,
             byte hour,
             byte date,
             byte month,
             UInt16 year);

        [DllImport("xfic2.dll",
            CharSet = CharSet.Unicode,
            CallingConvention = CallingConvention.StdCall,
            BestFitMapping = true,
            ExactSpelling = false,
            SetLastError = false,
            ThrowOnUnmappableChar = false)]
        public static extern  void DecodeUserHoldTime(UInt32 uht,
            ref byte sec,
            ref byte min,
            ref byte hour,
            ref byte date,
            ref byte month,
            ref UInt16 year);

        [DllImport("xfic2.dll")]
        public static extern  int CmdSetWorkingshift(int workingShiftNum, WorkingShift workingShift);

        [DllImport("xfic2.dll")]
        public static extern  int CmdReadWorkingshift(int workingShiftNum, ref WorkingShift workingShift);

        [DllImport("xfic2.dll")]
        public static extern  int CmdReadLogDuringDate(SyDate formDate, SyDate toDate, ref int logStartIndex, ref int LogCnt);

        [DllImport("xfic2.dll", CharSet = CharSet.Ansi)]
        public static extern  int CmdOpenDeviceByUsb(string wcsDevName, uint devAddr, uint password, byte traceIO);

        [DllImport("xfic2.dll")]
        public static extern int CmdSetAlarm(int dayOfWeek, SYS_BELL pSysBell);

        [DllImport("xfic2.dll")]
        public static extern int CmdReadAlarm(int dayOfWeek, ref SYS_BELL pSysBell);


        [DllImport("xfic2.dll", CharSet = CharSet.Unicode)]
        public static extern Boolean OpenCom(string port, int devAddr, uint baudrate);
    }
}