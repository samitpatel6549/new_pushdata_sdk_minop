using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Windows.Forms;
using System.Configuration;

namespace SaaSPushTech
{
    public static class ClsGlobal
    {
        public static int TransCount = 0;
        public static int ServerPort_BioTime6;
        public static int ServerPort_BioT5;
        public static int ServerPort_BioFace;
        public static int ServerPort_BioWeb;

        public static string LocalConStr = "";
        public static string LocalConStrMySql = "";
        public static string SMSAPIUrl = "http://smsapi.mantratecapp.com/Service.svc/mantra/";

        public static void WriteDeveloperSucessLog(string ErrorMsg)
        {
            StreamWriter sw = null;
            try
            {
                if (!Directory.Exists(Application.StartupPath + @"\DeveloperSucessLog\"))
                {
                    Directory.CreateDirectory(Application.StartupPath + @"\DeveloperSucessLog\");
                }

                sw = new StreamWriter(Application.StartupPath + "\\DeveloperSucessLog\\SucessLog.txt", true);
                sw.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + ": " + ErrorMsg);
                sw.Flush();
                sw.Close();
            }
            catch (Exception ex)
            {
            }
        }
        public static void WriteDeveloperErrorLog(string ErrorMsg)
        {
            StreamWriter sw = null;
            try
            {
                if (!Directory.Exists(Application.StartupPath + @"\DeveloperErrorLog\"))
                {
                    Directory.CreateDirectory(Application.StartupPath + @"\DeveloperErrorLog\");
                }

                sw = new StreamWriter(Application.StartupPath + "\\DeveloperErrorLog\\ErrorLog.txt", true);
                sw.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + ": " + ErrorMsg);
                sw.Flush();
                sw.Close();
            }
            catch (Exception ex)
            {
            }
        }
        public static void WriteErrorLog(string ErrorMsg)
        {
            StreamWriter sw = null;
            try
            {
                if (!Directory.Exists(Application.StartupPath + @"\PushDataServiceError\"))
                {
                    Directory.CreateDirectory(Application.StartupPath + @"\PushDataServiceError\");
                }

                sw = new StreamWriter(Application.StartupPath + "\\PushDataServiceError\\ErrorLog.txt", true);
                sw.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + ": " + ErrorMsg);
                sw.Flush();
                sw.Close();
            }
            catch(Exception ex)
            {
            }
        }
        public static void ErrorXml(string ErrorMsg)
        {
           
            StreamWriter sw = null;
            try
            {
                if (!Directory.Exists(Application.StartupPath + @"\ErrorXml\"))
                {
                    Directory.CreateDirectory(Application.StartupPath + @"\ErrorXml\");
                }

                sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "ErrorXml\\" + DateTime.Now.ToString("yyyyMMdd") + ".txt", true);
                sw.WriteLine(ErrorMsg);
                sw.Flush();
                sw.Close();
            }
            catch
            {
            }
        }
        public static void WriteEventLog(string EventMsg)
        {
            StreamWriter sw = null;
            try
            {
                if (!Directory.Exists(Application.StartupPath + @"\PushDataServiceEvent\"))
                {
                    Directory.CreateDirectory(Application.StartupPath + @"\PushDataServiceEvent\");
                }

                sw = new StreamWriter(Application.StartupPath + "\\PushDataServiceEvent\\EventLog.txt", true);
                sw.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + ": " + EventMsg);
                sw.Flush();
                sw.Close();
            }
            catch
            {
            }
        }
        public static void WriteSayHello(string AllTrans)
        {
            StreamWriter sw = null;
            try
            {
                if (!Directory.Exists(Application.StartupPath + @"\SayHelloDevice\"))
                {
                    Directory.CreateDirectory(Application.StartupPath + @"\SayHelloDevice\");
                }

                sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "SayHelloDevice\\" + DateTime.Now.ToString("yyyyMMdd") + ".txt", true);
                sw.WriteLine(AllTrans);
                sw.Flush();
                sw.Close();
            }
            catch
            {
            }
        }
        public static void WriteAllTrans(string AllTrans)
        {
            StreamWriter sw = null;
            try
            {
                if (!Directory.Exists(Application.StartupPath + @"\AllTrans\"))
                {
                    Directory.CreateDirectory(Application.StartupPath + @"\AllTrans\");
                }

                sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "AllTrans\\" + DateTime.Now.ToString("yyyyMMdd") + ".txt", true);
                sw.WriteLine(AllTrans);
                sw.Flush();
                sw.Close();
            }
            catch
            {
            }
        }
        public static void WriteAllMessage(string AllTrans)
        {
            StreamWriter sw = null;
            try
            {
                if (!Directory.Exists(Application.StartupPath + @"\AllMessage\"))
                {
                    Directory.CreateDirectory(Application.StartupPath + @"\AllMessage\");
                }

                sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "AllMessage\\" + DateTime.Now.ToString("yyyyMMdd") + ".txt", true);
                sw.WriteLine(AllTrans);
                sw.Flush();
                sw.Close();
            }
            catch
            {
            }
        }

        public static void WriteTranscId(string AllTrans)
        {
            StreamWriter sw = null;
            try
            {
                if (!Directory.Exists(Application.StartupPath + @"\PassOkdevice\"))
                {
                    Directory.CreateDirectory(Application.StartupPath + @"\PassOkdevice\");
                }

                sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "PassOkdevice\\" + DateTime.Now.ToString("yyyyMMdd") + ".txt", true);
                sw.WriteLine(AllTrans);
                sw.Flush();
                sw.Close();
            }
            catch
            {
            }
        }
        public static void WriteFailTrans(string FailTrans)
        {
            StreamWriter sw = null;
            try
            {
                if (!Directory.Exists(Application.StartupPath + @"\FailTrans\"))
                {
                    Directory.CreateDirectory(Application.StartupPath + @"\FailTrans\");
                }

                sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "FailTrans\\" + DateTime.Now.ToString("yyyyMMdd") + ".txt", true);
                sw.WriteLine(FailTrans);
                sw.Flush();
                sw.Close();
            }
            catch
            {
            }
        }
        public static void WriteSuccessTrans(string SuccessTrans)
        {
            StreamWriter sw = null;
            try
            {
                if (!Directory.Exists(Application.StartupPath + @"\SuccessTrans\"))
                {
                    Directory.CreateDirectory(Application.StartupPath + @"\SuccessTrans\");
                }

                sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "SuccessTrans\\" + DateTime.Now.ToString("yyyyMMdd") + ".txt", true);
                sw.WriteLine(SuccessTrans);
                sw.Flush();
                sw.Close();
            }
            catch
            {
            }
        }
        
        public static void WriteImportTransError(string ImportTransError)
        {
            StreamWriter sw = null;
            try
            {
                if (!Directory.Exists(Application.StartupPath + @"\ImportTransError\"))
                {
                    Directory.CreateDirectory(Application.StartupPath + @"\ImportTransError\");
                }

                sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "ImportTransError\\" + DateTime.Now.ToString("yyyyMMdd") + ".txt", true);
                sw.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + ": " + ImportTransError);
                sw.Flush();
                sw.Close();
            }
            catch
            {
            }
        }
        public static void WriteImportTransEvent(string ImportTransEvent)
        {
            StreamWriter sw = null;
            try
            {
                if (!Directory.Exists(Application.StartupPath + @"\ImportTransEvent\"))
                {
                    Directory.CreateDirectory(Application.StartupPath + @"\ImportTransEvent\");
                }

                sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "ImportTransEvent\\" + DateTime.Now.ToString("yyyyMMdd") + ".txt", true);
                sw.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + ": " + ImportTransEvent);
                sw.Flush();
                sw.Close();
            }
            catch
            {
            }
        }
        public static void WriteJunkData(string AllTrans)
        {
            StreamWriter sw = null;
            try
            {
                if (!Directory.Exists(Application.StartupPath + @"\JunkData\"))
                {
                    Directory.CreateDirectory(Application.StartupPath + @"\JunkData\");
                }

                sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "JunkData\\" + DateTime.Now.ToString("yyyyMMdd") + ".txt", true);
                sw.WriteLine(AllTrans);
                sw.Flush();
                sw.Close();
            }
            catch
            {
            }
        }

        public static void WriteErrorEventTypeTrans(string AllTrans)
        {
            StreamWriter sw = null;
            try
            {
                if (!Directory.Exists(Application.StartupPath + @"\ErrorEventType\"))
                {
                    Directory.CreateDirectory(Application.StartupPath + @"\ErrorEventType\");
                }

                sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "ErrorEventType\\" + DateTime.Now.ToString("yyyyMMdd") + ".txt", true);
                sw.WriteLine(AllTrans);
                sw.Flush();
                sw.Close();
            }
            catch
            {
            }
        }
       
        
        ///******************************************************************/
        ///*                            Function                            */
        ///******************************************************************/

        public const string gstrNoDevice = "No Device";
        ///******************************************************************/
        ///*                            Constant                            */
        ///******************************************************************/
        ////=============== Backup Number Constant ===============//
        // Finger 0
        public const short BACKUP_FP_0 = 0;
        // Finger 1
        public const short BACKUP_FP_1 = 1;
        // Finger 2
        public const short BACKUP_FP_2 = 2;
        // Finger 3
        public const short BACKUP_FP_3 = 3;
        // Finger 4
        public const short BACKUP_FP_4 = 4;
        // Finger 5
        public const short BACKUP_FP_5 = 5;
        // Finger 6
        public const short BACKUP_FP_6 = 6;
        // Finger 7
        public const short BACKUP_FP_7 = 7;
        // Finger 8
        public const short BACKUP_FP_8 = 8;
        // Finger 9
        public const short BACKUP_FP_9 = 9;
        // Password
        public const short BACKUP_PSW = 10;
        // Card
        public const short BACKUP_CARD = 11;


        // Door Open
        public const short LOG_OPEN_DOOR = 32;
        // Door Open as threat
        public const short LOG_OPEN_THREAT = 48;

        ////=============== IOMode of GeneralLogData ===============//
        public const short LOG_IOMODE_IN = 0;
        public const short LOG_IOMODE_OUT = 1;
        // = LOG_IOMODE_IO
        public const short LOG_IOMODE_OVER_IN = 2;

        public const short LOG_IOMODE_OVER_OUT = 3;
        ////=============== Error code ===============//
        public const short RUN_SUCCESS = 1;
        public const short RUNERR_NOSUPPORT = 0;
        public const short RUNERR_UNKNOWNERROR = -1;
        public const short RUNERR_NO_OPEN_COMM = -2;
        public const short RUNERR_WRITE_FAIL = -3;
        public const short RUNERR_READ_FAIL = -4;
        public const short RUNERR_INVALID_PARAM = -5;
        public const short RUNERR_NON_CARRYOUT = -6;
        public const short RUNERR_DATAARRAY_END = -7;
        public const short RUNERR_DATAARRAY_NONE = -8;
        public const short RUNERR_MEMORY = -9;
        public const short RUNERR_MIS_PASSWORD = -10;
        public const short RUNERR_MEMORYOVER = -11;
        public const short RUNERR_DATADOUBLE = -12;
        public const short RUNERR_MANAGEROVER = -14;

        public const short RUNERR_FPDATAVERSION = -15;
        public static string ReturnResultPrint(ref int anResultCode)
        {
            string functionReturnValue = null;
            switch (anResultCode)
            {
                case RUN_SUCCESS:
                    functionReturnValue = "Successful!";
                    break;
                case RUNERR_NOSUPPORT:
                    functionReturnValue = "No support";
                    break;
                case RUNERR_UNKNOWNERROR:
                    functionReturnValue = "Unknown error";
                    break;
                case RUNERR_NO_OPEN_COMM:
                    functionReturnValue = "No Open Comm";
                    break;
                case RUNERR_WRITE_FAIL:
                    functionReturnValue = "Write Error";
                    break;
                case RUNERR_READ_FAIL:
                    functionReturnValue = "Read Error";
                    break;
                case RUNERR_INVALID_PARAM:
                    functionReturnValue = "Parameter Error";
                    break;
                case RUNERR_NON_CARRYOUT:
                    functionReturnValue = "execution of command failed";
                    break;
                case RUNERR_DATAARRAY_END:
                    functionReturnValue = "End of data";
                    break;
                case RUNERR_DATAARRAY_NONE:
                    functionReturnValue = "Nonexistence data";
                    break;
                case RUNERR_MEMORY:
                    functionReturnValue = "Memory Allocating Error";
                    break;
                case RUNERR_MIS_PASSWORD:
                    functionReturnValue = "License Error";
                    break;
                case RUNERR_MEMORYOVER:
                    functionReturnValue = "full enrolldata & can`t put enrolldata";
                    break;
                case RUNERR_DATADOUBLE:
                    functionReturnValue = "this ID is already  existed.";
                    break;
                case RUNERR_MANAGEROVER:
                    functionReturnValue = "full manager & can`t put manager.";
                    break;
                case RUNERR_FPDATAVERSION:
                    functionReturnValue = "mistake fp data version.";

                    break;
                default:
                    functionReturnValue = "Unknown error";
                    break;
            }
            return functionReturnValue;
        }

        public static void WriteSchoolSucessLog(string ErrorMsg)
        {
            StreamWriter sw = null;
            try
            {
                if (!Directory.Exists(Application.StartupPath + @"\SchoolSucessLog\"))
                {
                    Directory.CreateDirectory(Application.StartupPath + @"\SchoolSucessLog\");
                }

                sw = new StreamWriter(Application.StartupPath + "\\SchoolSucessLog\\SucessLog.txt", true);
                sw.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + ": " + ErrorMsg);
                sw.Flush();
                sw.Close();
            }
            catch (Exception ex)
            {
            }
        }
        public static void WriteSchoolErrorLog(string ErrorMsg)
        {
            StreamWriter sw = null;
            try
            {
                if (!Directory.Exists(Application.StartupPath + @"\SchoolErrorLog\"))
                {
                    Directory.CreateDirectory(Application.StartupPath + @"\SchoolErrorLog\");
                }

                sw = new StreamWriter(Application.StartupPath + "\\SchoolErrorLog\\ErrorLog.txt", true);
                sw.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + ": " + ErrorMsg);
                sw.Flush();
                sw.Close();
            }
            catch (Exception ex)
            {
            }
        }

    }
}
