﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.Xml;
using System.Data;
using Mantra.SMS.Lib;
using SaaSPushTech;
using Newtonsoft.Json;

namespace SaaSPushTech
{
    // Terminal class
    class Terminal_old : IDisposable
    {
        public Boolean disposed;		// Is terminal disposed?
        public TcpClient client;		// Client object of terminal
        public NetworkStream stream;	// Network stream
        public Timer timerAlive;		// Alive watch timer
        public Byte[] message;			// Message buffer
        public Byte[] messageAccumulated;// Accumulated message buffer
        public int messageAccumulatedLen;// Accumulated message length

        private const int MaxMessageSize = 2048 + 8 * 1024 * 2;		// Maximum message size // 18K
        private const int PingTimeout = 30 * 1000;	// Ping timeout

        // Clean up client
        private void CleanUp(Boolean disposing)
        {
            if (this.disposed)
                return;

            this.disposed = true;

            if (disposing)
            {
                // Dispose client objects
                timerAlive.Change(Timeout.Infinite, Timeout.Infinite);
                try
                {
                    stream.Close();
                    client.Close();
                }
                catch { }
            }
        }

        public void Dispose()
        {
            CleanUp(true);
        }

        public Terminal_old()
        {
            disposed = false;

            // Message buffer
            message = new Byte[MaxMessageSize];
            messageAccumulated = new Byte[MaxMessageSize];
            messageAccumulatedLen = 0;

            timerAlive = new Timer(
                new TimerCallback(this.OnAliveTimerExpired));
            RestartAliveTimer();
        }

        ~Terminal_old()
        {
            CleanUp(false);
        }

        // When alive timer is expired
        public void OnAliveTimerExpired(Object stateInfo)
        {
            this.Dispose();
        }

        // Restart alive timer
        public void RestartAliveTimer()
        {
            timerAlive.Change(PingTimeout, Timeout.Infinite);
        }

        // Establish connection to terminal
        public void EstablishConnect(TcpClient client_)
        {
            client = client_;
            stream = client.GetStream();

            RestartAliveTimer();
            stream.BeginRead(message, 0, MaxMessageSize,
                new AsyncCallback(Terminal_old.OnReceive), this);
        }

        // Send the stream to terminal
        public static void OnSend(IAsyncResult iar)
        {
            Terminal_old term = (Terminal_old)iar.AsyncState;
            try
            {
                term.stream.EndWrite(iar);
            }
            catch
            {
            }
        }

        // Predicate for end of message
        private static Boolean PreEndMessage(Byte c)
        {
            return (c == 0);
        }

        private string GetElementValue(XmlDocument doc, string elementName)
        {
            foreach (XmlElement x in doc.DocumentElement.ChildNodes)
            {
                if (x.Name == elementName)
                    return x.InnerText;
            }
            throw new Exception();
        }
        // Process a message
        private void ProcMessage(string message)
        {
            
            XmlDocument doc = new XmlDocument();


            //ClsGlobal.WriteErrorLog("ProcMessage : 1");

            const ulong invalid_val64 = 0xFFFFFFFFFFFFFFFF;
            const uint invalid_val = 0xFFFFFFFF;
            string termType = null, serial = null, eventType = null, dispMessage = null;
            uint termID = invalid_val, transID = invalid_val;
            uint year = invalid_val, month = invalid_val, day = invalid_val,
                hour = invalid_val, minute = invalid_val, second = invalid_val;
            try
            {
                doc.Load(new StringReader(message));

            }
            catch (Exception ex)
            {
                ClsGlobal.ErrorXml("Error in XML : " + message + " " + ex.Message);
            }


            // Terminal type
            try
            {
                termType = GetElementValue(doc, "TerminalType");
            }
            catch (System.Exception)
            {
                termType = "";
            }

            // Terminal ID
            try
            {
                termID = uint.Parse(GetElementValue(doc, "TerminalID"));
            }
            catch (System.Exception) { }

            // Serial Number
            try
            {
                serial = GetElementValue(doc, "DeviceSerialNo");
            }
            catch (System.Exception) { }

            // Transaction ID
            try
            {
                transID = uint.Parse(GetElementValue(doc, "TransID"));
            }
            catch (System.Exception) { }

            // Event
            try
            {
                eventType = GetElementValue(doc, "Event");
            }

            catch (System.Exception) { }

            if (eventType == "TimeLog")
            {
                ClsGlobal.WriteAllMessage(message);
            }

            // Termianl Type
            dispMessage = "[" + termType + ":";

            // Terminal ID
            if (termID != invalid_val)
                dispMessage += termID.ToString();

            // Serial Number
            if (serial != null)
                dispMessage += " SN=" + serial + "] ";
            else
                dispMessage += "] ";

            // Event Type
            if (eventType != null)
                dispMessage += eventType;

            string attendStat = null, verifMode = null,
                apStat = null, photo = null, action = null, alarmType = null, LogImage = null;
            Byte[] photo_data = null;
            ulong userID = invalid_val64, adminID = invalid_val64;
            uint actionStat = invalid_val, doorID = invalid_val, jobCode = invalid_val;
            string msgReply = "<?xml version=\"1.0\"?><Message>";

            Int64 EnrollNo = 0;
            DateTime PunchTime = DateTime.Now;
            string Trans = "";

            switch (eventType)
            {
                case "TimeLog":
                    // Date and time of log
                    // Year
                    try
                    {
                        year = uint.Parse(GetElementValue(doc, "Year"));
                    }
                    catch (System.Exception) { }

                    // Month
                    try
                    {
                        month = uint.Parse(GetElementValue(doc, "Month"));
                    }
                    catch (System.Exception) { }

                    // Day
                    try
                    {
                        day = uint.Parse(GetElementValue(doc, "Day"));
                    }
                    catch (System.Exception) { }

                    // Hour
                    try
                    {
                        hour = uint.Parse(GetElementValue(doc, "Hour"));
                    }
                    catch (System.Exception) { }

                    // Minute
                    try
                    {
                        minute = uint.Parse(GetElementValue(doc, "Minute"));
                    }
                    catch (System.Exception) { }

                    // Second
                    try
                    {
                        second = uint.Parse(GetElementValue(doc, "Second"));
                    }
                    catch (System.Exception) { }
                    PunchTime = new DateTime(Convert.ToInt32(year), Convert.ToInt32(month), Convert.ToInt32(day), Convert.ToInt32(hour), Convert.ToInt32(minute), Convert.ToInt32(second));

                    // User ID
                    try
                    {
                        userID = ulong.Parse(GetElementValue(doc, "UserID"));
                    }
                    catch (System.Exception) { }


                    // Door ID
                    try
                    {
                        doorID = uint.Parse(GetElementValue(doc, "DoorID"));
                    }
                    catch (System.Exception) { }

                    // Time attendance status
                    try
                    {
                        attendStat = GetElementValue(doc, "AttendStat");
                    }
                    catch (System.Exception) { }

                    // Verification mode
                    try
                    {
                        verifMode = GetElementValue(doc, "VerifMode");
                    }
                    catch (System.Exception) { }

                    // Job code
                    try
                    {
                        jobCode = uint.Parse(GetElementValue(doc, "JobCode"));
                    }
                    catch (System.Exception) { }

                    // Antipass status
                    try
                    {
                        apStat = GetElementValue(doc, "APStat");
                    }
                    catch (System.Exception) { }

                    // Photo taken
                    try
                    {
                        photo = GetElementValue(doc, "Photo");
                    }
                    catch (System.Exception) { }

                    try
                    {
                        LogImage = GetElementValue(doc, "LogImage");
                        if (LogImage != null)
                            photo_data = Convert.FromBase64String(LogImage);
                    }



                    catch (System.Exception) { }

                    if (transID != invalid_val)
                        dispMessage += "(" + transID.ToString() + ") ";
                    if (year != invalid_val && month != invalid_val && day != invalid_val &&
                        hour != invalid_val && minute != invalid_val && second != invalid_val)
                        dispMessage += " AT " + year.ToString() + "/" + month.ToString() + "/" + day.ToString() + " " +
                            hour.ToString() + ":" + minute.ToString() + ":" + second.ToString() + ", ";
                    if (userID != invalid_val64)
                        dispMessage += "UserID=" + String.Format("{0}, ", userID);
                    if (doorID != invalid_val)
                        dispMessage += "DoorID=" + doorID.ToString() + ", ";
                    if (attendStat != null)
                        dispMessage += "AttendStat=" + attendStat + ", ";
                    if (verifMode != null)
                        dispMessage += "VerifMode=" + verifMode + ", ";
                    if (jobCode != invalid_val)
                        dispMessage += "JobCode=" + jobCode.ToString() + ", ";
                    if (apStat != null)
                        dispMessage += "APStatus=" + apStat + ", ";
                    if (photo != null)
                        dispMessage += "PhotoTaken=" + photo + ", ";
                    if (photo_data != null)
                        dispMessage += "LogImage=(" + Convert.ToString(photo_data.Length) + "bytes)";

                    // Concatenate a message to send back
                    msgReply += "<Request>UploadedLog</Request><TransID>" +
                        transID.ToString() + "</TransID></Message>";
                    ClsGlobal.WriteAllMessage("Case Timelogend: " + transID.ToString());
                    break;

                case "AdminLog":
                    // Date and time of log
                    // Year
                    try
                    {
                        year = uint.Parse(GetElementValue(doc, "Year"));
                    }
                    catch (System.Exception) { }

                    // Month
                    try
                    {
                        month = uint.Parse(GetElementValue(doc, "Month"));
                    }
                    catch (System.Exception) { }

                    // Day
                    try
                    {
                        day = uint.Parse(GetElementValue(doc, "Day"));
                    }
                    catch (System.Exception) { }

                    // Hour
                    try
                    {
                        hour = uint.Parse(GetElementValue(doc, "Hour"));
                    }
                    catch (System.Exception) { }

                    // Minute
                    try
                    {
                        minute = uint.Parse(GetElementValue(doc, "Minute"));
                    }
                    catch (System.Exception) { }

                    // Second
                    try
                    {
                        second = uint.Parse(GetElementValue(doc, "Second"));
                    }
                    catch (System.Exception) { }

                    // Administrator ID
                    try
                    {
                        adminID = ulong.Parse(GetElementValue(doc, "AdminID"));
                    }
                    catch (System.Exception) { }

                    // User ID
                    try
                    {
                        userID = ulong.Parse(GetElementValue(doc, "UserID"));
                    }
                    catch (System.Exception) { }

                    // Action
                    try
                    {
                        action = GetElementValue(doc, "Action");
                    }
                    catch (System.Exception) { }

                    // Action status
                    try
                    {
                        actionStat = uint.Parse(GetElementValue(doc, "Stat"));
                    }
                    catch (System.Exception) { }

                    if (transID != invalid_val)
                        dispMessage += "(" + transID.ToString() + ") ";
                    if (year != invalid_val && month != invalid_val && day != invalid_val &&
                        hour != invalid_val && minute != invalid_val && second != invalid_val)
                        dispMessage += " AT " + year.ToString() + "/" + month.ToString() + "/" + day.ToString() + " " +
                            hour.ToString() + ":" + minute.ToString() + ":" + second.ToString() + ", ";
                    if (adminID != invalid_val64)
                        dispMessage += "AdminID=" + String.Format("{0}, ", adminID);
                    if (userID != invalid_val64)
                        dispMessage += "UserID=" + String.Format("{0}, ", userID);
                    if (action != null)
                        dispMessage += "Action=" + action + ", ";
                    if (actionStat != invalid_val)
                        dispMessage += "Status=" + String.Format("{0:D}", actionStat);

                    // Concatenate a message to send back
                    msgReply += "<Request>UploadedLog</Request><TransID>" +
                        transID.ToString() + "</TransID></Message>";
                    Trans = serial + "," + Convert.ToInt32(userID) + "," + PunchTime.ToString("yyyy-MM-dd HH:mm:ss") + ",'IN'," + termID.ToString() + "," +
                        System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                    break;

                case "Alarm":
                    // Year
                    try
                    {
                        year = uint.Parse(GetElementValue(doc, "Year"));
                    }
                    catch (System.Exception) { }

                    // Month
                    try
                    {
                        month = uint.Parse(GetElementValue(doc, "Month"));
                    }
                    catch (System.Exception) { }

                    // Day
                    try
                    {
                        day = uint.Parse(GetElementValue(doc, "Day"));
                    }
                    catch (System.Exception) { }

                    // Hour
                    try
                    {
                        hour = uint.Parse(GetElementValue(doc, "Hour"));
                    }
                    catch (System.Exception) { }

                    // Minute
                    try
                    {
                        minute = uint.Parse(GetElementValue(doc, "Minute"));
                    }
                    catch (System.Exception) { }

                    // Second
                    try
                    {
                        second = uint.Parse(GetElementValue(doc, "Second"));
                    }
                    catch (System.Exception) { }

                    // User ID
                    try
                    {
                        userID = ulong.Parse(GetElementValue(doc, "UserID"));
                    }
                    catch (System.Exception) { }

                    // Door ID
                    try
                    {
                        doorID = uint.Parse(GetElementValue(doc, "DoorID"));
                    }
                    catch (System.Exception) { }

                    // Time attendance status
                    try
                    {
                        alarmType = GetElementValue(doc, "Type");
                    }
                    catch (System.Exception) { }

                    if (transID != invalid_val)
                        dispMessage += "(" + transID.ToString() + ") ";
                    if (year != invalid_val && month != invalid_val && day != invalid_val &&
                        hour != invalid_val && minute != invalid_val && second != invalid_val)
                        dispMessage += " AT " + year.ToString() + "/" + month.ToString() + "/" + day.ToString() + " " +
                            hour.ToString() + ":" + minute.ToString() + ":" + second.ToString() + ", ";
                    if (userID != invalid_val64)
                        dispMessage += "UserID=" + String.Format("{0}, ", userID);
                    if (doorID != invalid_val)
                        dispMessage += "DoorID=" + doorID.ToString() + ", ";
                    if (alarmType != null)
                        dispMessage += "Type=" + alarmType;

                    // Concatenate a message to send back
                    msgReply += "<Request>UploadedLog</Request><TransID>" +
                        transID.ToString() + "</TransID></Message>";

                    break;

                case "KeepAlive":
                    // Concatenate a message to send back
                    msgReply += "<Request>KeptAlive</Request><TransID>" +
                        transID.ToString() + "</TransID></Message>";



                    break;
            }



            // Display the message on the form
            //MainForm.theForm.BeginInvoke(
            //    MainForm.theForm.theDelegateAppendMessage, dispMessage);

            //Insert Success Transaction - SQL
            string ErrorMsg = "";
            string VInOut = "";
            Int32 retVal = 0;

            if (eventType == "KeepAlive")
            {
                //ClsGlobal.WriteErrorLog("KeepAlive : "); 
                ClsBALTblTransaction ObjClsBALTblTransaction = new ClsBALTblTransaction();
                //ObjClsBALTblTransaction.SayHello(Convert.ToInt64(termID), ref ErrorMsg);
                ObjClsBALTblTransaction.SayHello((serial), ref ErrorMsg);

                // Reply the message
                byte[] buffer = new byte[msgReply.Length + 1];
                System.Text.Encoding.ASCII.GetBytes(msgReply).CopyTo(buffer, 0);
                buffer[msgReply.Length] = 0;         // Insert null character as end token of message
                stream.BeginWrite(buffer,
                    0, buffer.Length, new AsyncCallback(Terminal_old.OnSend), this);
            }
            else if (eventType == "TimeLog")
            {
                EnrollNo = Convert.ToInt32(userID);

                switch (attendStat)
                {
                    case "Duty On":
                        VInOut = "IN";
                        break;
                    case "Duty Off":
                        VInOut = "OUT";
                        break;
                    case "DutyOn":
                        VInOut = "IN";
                        break;
                    case "DutyOff":
                        VInOut = "OUT";
                        break;
                    case "GoOut":
                        VInOut = "OUT";
                        break;
                    case "GoIn":
                        VInOut = "IN";
                        break;
                    case "Out":
                        VInOut = "OUT";
                        break;
                    case "In":
                        VInOut = "IN";
                        break;
                    case "Return":
                        VInOut = "IN";
                        break;
                    case "Overtime Off":
                        VInOut = "OUT";
                        break;
                    case "Overtime On":
                        VInOut = "IN";
                        break;
                    case "OvertimeOff":
                        VInOut = "OUT";
                        break;
                    case "OvertimeOn":
                        VInOut = "IN";
                        break;
                    default:
                        VInOut = "IN";
                        break;
                }

                try
                {
                    ClsGlobal.WriteAllMessage("before alltrans: " + transID.ToString());

                    Trans = serial + "," + EnrollNo + "," + PunchTime.ToString("yyyy-MM-dd HH:mm:ss") + "," + VInOut + "," + termID.ToString() + "," +
                        System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

                    if (EnrollNo != 134217727)
                    {
                        ClsGlobal.WriteAllTrans(Trans);
                    }
                    else
                    {
                        //junk data 
                        //ClsGlobal.WriteJunkData(Trans);
                    }

                    ClsBALTblTransaction ObjClsBALTblTransaction = new ClsBALTblTransaction();
                    ObjClsBALTblTransaction.EnrollNo = EnrollNo;
                    ObjClsBALTblTransaction.InOutMode = VInOut;
                    ObjClsBALTblTransaction.PunchTime = Convert.ToDateTime(PunchTime.ToString("yyyy-MM-dd HH:mm:ss"));
                    ObjClsBALTblTransaction.DeviceIP = "127.0.0.1";
                    ObjClsBALTblTransaction.DeviceID = Convert.ToInt32(termID);
                    ObjClsBALTblTransaction.DeviceSerialNo = serial; //get serialno of device


                    if (!string.IsNullOrEmpty(serial))
                    {

                        if (EnrollNo != 134217727)
                        {
                            retVal = ObjClsBALTblTransaction.OnSave(ref ErrorMsg);
                        }
                        else
                        {
                            retVal = 1;
                        }

                    }
                    else
                    {
                        retVal = 1;
                    }

                    //else
                    //{
                    //    retVal = 1;
                    //    //ClsGlobal.WriteFailTrans("else case EnrollNo " + EnrollNo.ToString());//check for wrong data
                    //}

                    if (retVal == 1 || retVal == 2 || retVal == 3)
                    {
                        // Reply the message
                        //ClsGlobal.WriteFailTrans("With retVal EnrollNo " + retVal.ToString() + "-" + EnrollNo.ToString() + " srNo=" + serial .ToString()+ "msgreplay=" + msgReply.ToString());//check for wrong data
                        byte[] buffer = new byte[msgReply.Length + 1];
                        System.Text.Encoding.ASCII.GetBytes(msgReply).CopyTo(buffer, 0);
                        buffer[msgReply.Length] = 0;         // Insert null character as end token of message
                        stream.BeginWrite(buffer,
                            0, buffer.Length, new AsyncCallback(Terminal_old.OnSend), this);
                        
                        //ClsGlobal.TransCount = ClsGlobal.TransCount + 1;

                        FrmLog.theForm.BeginInvoke(FrmLog.theForm.theDelegateAppendMessage, ClsGlobal.TransCount.ToString());
                        if (retVal == 1 || retVal == 2)
                        {

                            if (EnrollNo != 134217727)
                            {
                                ClsGlobal.TransCount = ClsGlobal.TransCount + 1;
                                ClsGlobal.WriteSuccessTrans(Trans);
                            }
                        }
                        else
                        {
                            if (EnrollNo != 134217727)
                            {
                                ClsGlobal.WriteFailTrans("NA PID" + " " + Trans);
                            }
                            retVal = 1;

                        }
                        
                    }
                    else if (retVal != 1 && retVal != 2)
                    {
                        if (EnrollNo != 134217727)
                        {
                            ClsGlobal.WriteFailTrans(Trans);
                        }
                        
                    }
                }
                catch (Exception ex)
                {
                    ClsGlobal.WriteErrorLog("ProcMessage : " + serial + " " + ex.Message);
                }

            }
            else
            {
                //ClsGlobal.WriteErrorEventTypeTrans(eventType + " " +  Trans);
                // Reply the message
                if (eventType == "AdminLog")
                {

                    ClsGlobal.WriteTranscId("AdminLog pass 1 to device transid: " + transID.ToString());
                    byte[] buffer = new byte[msgReply.Length + 1];
                    System.Text.Encoding.ASCII.GetBytes(msgReply).CopyTo(buffer, 0);
                    buffer[msgReply.Length] = 0;         // Insert null character as end token of message
                    stream.BeginWrite(buffer,
                        0, buffer.Length, new AsyncCallback(Terminal_old.OnSend), this);
                }
            }
        }

        public Boolean ProcStream(out Int32 consumed)
        {
            consumed = 0;
            Byte[] data = messageAccumulated;
            int size = messageAccumulatedLen;

            if (messageAccumulatedLen == MaxMessageSize)
                return false;

            Int32 end = Array.FindIndex(data, 0, messageAccumulatedLen, PreEndMessage);
            if (end == -1)
            {
                consumed = 0;
                return true;
            }

            ProcMessage(System.Text.Encoding.ASCII.GetString(data, 0, end));

            for (; end < messageAccumulatedLen; end++)
            {
                if (data[end] != 0)
                    break;
            }
            if (end != messageAccumulatedLen)
                end++;
            consumed = end;

            return true;
        }

        public static void OnReceive(IAsyncResult iar)
        {
            Terminal_old term = (Terminal_old)iar.AsyncState;
            //ClsGlobal.WriteErrorLog("OnReceive : " + iar);
            int rcv;
            try
            {
                rcv = term.stream.EndRead(iar);
                if (rcv <= 0)
                    throw new Exception("connection closed");

                // Accumulate message
                Array.Copy(term.message, 0,
                    term.messageAccumulated, term.messageAccumulatedLen, rcv);
                term.messageAccumulatedLen += rcv;

                while (term.messageAccumulatedLen > 0)
                {
                    int consumed;

                    // Process stream
                    if (!term.ProcStream(out consumed))
                        throw new Exception("handle failed");

                    // Trim consumed stream
                    if (consumed > 0)
                    {
                        term.messageAccumulatedLen -= consumed;
                        Array.Copy(term.messageAccumulated,
                            consumed, term.messageAccumulated,
                            0, term.messageAccumulatedLen);
                    }
                    else
                    {
                        break;
                    }
                }

                // Restart alive timer
                term.RestartAliveTimer();
                term.stream.BeginRead(term.message, 0, MaxMessageSize,
                    new AsyncCallback(Terminal_old.OnReceive), term);
            }
            catch
            {
                term.Dispose();
            }
        }

        public void PushDatasendSMS(Int64 EmpPunchID, DateTime PunchTime)
        {
            try
            {


                int EmpCode = 0;
                string UserID = "";
                string Password = "";
                string Template = "";
                string IsActive = "";
                string SMSAPI = ClsGlobal.SMSAPIUrl;
                int ret = 0;
                DateTime MIN_OUT_Time = DateTime.Now;
                string toDayDate = DateTime.Now.ToString("yyyy-MM-dd");

                ClsDALTransaction ObjCls = new ClsDALTransaction();
                DataSet EmpDs;
                EmpDs = ObjCls.GetAllEmpSMS(EmpPunchID);

                if (EmpDs != null)
                {
                    // SMSEventMaster
                    if (EmpDs.Tables[0] != null)
                    {
                        if (EmpDs.Tables[0].Rows.Count > 0)
                        {
                            // EmpMaster 
                            if (EmpDs.Tables[1] != null)
                            {
                                EmpCode = Convert.ToInt32(EmpDs.Tables[1].Rows[0]["Empcode"]);

                                //    check if already SMS sent or not 
                                DataSet dsCheck = ObjCls.CheckSMSSent(EmpCode, PunchTime);
                                if (dsCheck != null)
                                {
                                    if (dsCheck.Tables[0] != null && dsCheck.Tables[0].Rows.Count > 0)
                                    {

                                    }
                                    else
                                    {
                                        if (EmpDs.Tables[1].Rows.Count > 0)
                                        {
                                            // SMS cONFIGURATION - SMS Templates
                                            if (EmpDs.Tables[2] != null)
                                            {
                                                if (EmpDs.Tables[2].Rows.Count > 0)
                                                {
                                                    ClsEncryptionMain ObjEncDec = new ClsEncryptionMain();
                                                    UserID = ObjEncDec.DecryptPassword(EmpDs.Tables[2].Rows[0]["UserID"].ToString());
                                                    Password = ObjEncDec.DecryptPassword(EmpDs.Tables[2].Rows[0]["Password"].ToString());
                                                    IsActive = ObjEncDec.DecryptPassword(EmpDs.Tables[2].Rows[0]["IsActive"].ToString());
                                                    Template = Convert.ToString(EmpDs.Tables[2].Rows[0]["Template"]);

                                                    // Employee full detail by EmpCode
                                                    if (EmpDs.Tables[3] != null)
                                                    {
                                                        if (EmpDs.Tables[3].Rows.Count > 0)
                                                        {
                                                            Template = Template.Replace("EmpName", EmpDs.Tables[3].Rows[0]["EmpName"].ToString());
                                                            Template = Template.Replace("EmpID", EmpDs.Tables[3].Rows[0]["EmpID"].ToString());
                                                            Template = Template.Replace("EmpAddress", EmpDs.Tables[3].Rows[0]["EmpAddress"].ToString());
                                                            Template = Template.Replace("EmpPhNo", EmpDs.Tables[3].Rows[0]["EmpPhNo"].ToString());
                                                            Template = Template.Replace("EmpMNo", EmpDs.Tables[3].Rows[0]["EmpMNo"].ToString());
                                                            Template = Template.Replace("EmpEmail", EmpDs.Tables[3].Rows[0]["EmpEmail"].ToString());
                                                            Template = Template.Replace("EmpDOB", EmpDs.Tables[3].Rows[0]["EmpDOB"].ToString());
                                                            Template = Template.Replace("EmpJoinDate", EmpDs.Tables[3].Rows[0]["EmpJoinDate"].ToString());
                                                            Template = Template.Replace("EmpResignDate", EmpDs.Tables[3].Rows[0]["EmpResignDate"].ToString());
                                                            Template = Template.Replace("DeptName", EmpDs.Tables[3].Rows[0]["DeptName"].ToString());
                                                            Template = Template.Replace("DesgName", EmpDs.Tables[3].Rows[0]["DesgName"].ToString());
                                                            Template = Template.Replace("ShiftName", EmpDs.Tables[3].Rows[0]["ShiftName"].ToString());
                                                            Template = Template.Replace("InOutTime", PunchTime.ToString("yyyy-MM-dd HH:mm"));
                                                            //Template = Template.Replace("AttendDate", EmpDs.Tables[3].Rows[0]["AttendDate"].ToString());
                                                            Template = Template.Replace("CompanyName", EmpDs.Tables[3].Rows[0]["CompanyName"].ToString());
                                                        }
                                                        else
                                                        {
                                                            //ClsGlobal.WriteErrorLog("PayTimePushData : data not available in EmpMaster");
                                                        }
                                                    }
                                                    else
                                                    {
                                                        // ClsGlobal.WriteErrorLog("PayTimePushData : Error while getting data from EmpMaster");
                                                    }

                                                    // check API Active or not
                                                    if (IsActive == "1")
                                                    {
                                                        //  send msg
                                                        string Ntxn = "";
                                                        string Nrn = "";
                                                        string MErr = "";
                                                        ResSms deserializedResSms = null;
                                                        //  save Log into db
                                                        int b = ObjCls.OnSaveOnEveryPunch(EmpCode, PunchTime);
                                                        if (b > 0)
                                                        {
                                                            try
                                                            {
                                                                deserializedResSms = SendSMS(EmpCode, PunchTime, UserID, Password, Template, EmpDs.Tables[1].Rows[0]["MobNoSMS"].ToString(), SMSAPI, ref Nrn, ref Ntxn, ref MErr);
                                                                // over
                                                                if (deserializedResSms.ec == "0")
                                                                {
                                                                    if ((deserializedResSms.rn == Nrn) && (deserializedResSms.txn == Ntxn))
                                                                    {

                                                                        try
                                                                        {
                                                                            //  save SMS Log
                                                                            ret = ObjCls.OnSaveSMSLog("OnEveryPunch", EmpCode, 1, Template, deserializedResSms.ec, deserializedResSms.em, deserializedResSms.rtn);
                                                                            if (ret > 0)
                                                                            {
                                                                                //int b1 = ObjCls.OnSaveOnEveryPunch(EmpCode, PunchTime);
                                                                            }
                                                                            else
                                                                            {
                                                                                ClsGlobal.WriteErrorLog("OnEveryPunch data doesn't save in SMSLog");
                                                                            }
                                                                        }
                                                                        catch (Exception ex)
                                                                        {
                                                                            ClsGlobal.WriteErrorLog(ex.Message.ToString() + "PushData");
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        int b2 = ObjCls.OnDeleteOnEveryPunch(EmpCode, PunchTime.ToString("yyyy-MM-dd HH:mm:ss"));
                                                                        ret = ObjCls.OnSaveSMSLog("OnEveryPunch", EmpCode, 0, Template, deserializedResSms.ec, deserializedResSms.em, deserializedResSms.rtn);
                                                                        ClsGlobal.WriteErrorLog("rn Or Ntxn Not Match with API");
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    //delete
                                                                    int b2 = ObjCls.OnDeleteOnEveryPunch(EmpCode, PunchTime.ToString("yyyy-MM-dd HH:mm:ss"));
                                                                    if (MErr == "")
                                                                    {
                                                                        ret = ObjCls.OnSaveSMSLog("OnEveryPunch", EmpCode, 0, Template, deserializedResSms.ec, deserializedResSms.em, deserializedResSms.rtn);
                                                                    }
                                                                    else
                                                                    {
                                                                        ret = ObjCls.OnSaveSMSLog("OnEveryPunch", EmpCode, 0, Template, "-999", MErr, deserializedResSms.rtn);
                                                                    }
                                                                }
                                                            }
                                                            catch (Exception ex)
                                                            {
                                                                //delete
                                                                int b2 = ObjCls.OnDeleteOnEveryPunch(EmpCode, PunchTime.ToString("yyyy-MM-dd HH:mm:ss"));
                                                                ClsGlobal.WriteErrorLog("OnEveryPunch" + ex.Message.ToString());
                                                            }
                                                        }
                                                        else
                                                        {
                                                            //ClsGlobal.WriteErrorLog("data doesn't save in OnEveryPunch");
                                                        }
                                                    }
                                                    else
                                                    {
                                                        // ClsGlobal.WriteErrorLog("OnEveryPunch : API not active");
                                                    }
                                                }
                                                else
                                                {
                                                    //ClsGlobal.WriteErrorLog("PayTimePushData : data not available in Templates and API");
                                                }
                                            }
                                            else
                                            {
                                                //ClsGlobal.WriteErrorLog("PayTimePushData : Error while getting data from SMS Templates and API");
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    //ClsGlobal.WriteErrorLog("PayTimePushData : data not available in EmpMaster");
                                }
                            }
                            else
                            {
                                //ClsGlobal.WriteErrorLog("PayTimePushData : Error while getting data from EmpMaster");
                            }
                        }
                        else
                        {
                            //ClsGlobal.WriteErrorLog("PayTimePushData : data not available in SMSEventMaster");
                        }
                    }
                    else
                    {
                        //ClsGlobal.WriteErrorLog("PayTimePushData : Error while getting data from SMSEventMaster");
                    }
                }
                else
                {
                    //ClsGlobal.WriteErrorLog("PayTimePushData : Error while getting data from GetAllEmpSMS()");
                }
            }
            catch (Exception ex)
            {
                ClsGlobal.WriteErrorLog("PushDatasendSMS : " + ex.Message);
            }
        }

        public ResSms SendSMS(int empcode, DateTime PunchTime, string un, string pwd, string msg, string ton, string api, ref string Nrn, ref string Ntxn, ref string MErr)
        {
            ResSms deserializedResSms = new ResSms();
            ClsDALTransaction ObjCls = new ClsDALTransaction();
            try
            {
                WebClient wc = new WebClient();
                wc.Headers["Content-Type"] = "application/json";
                Random r = new Random();
                ReqSms req = new ReqSms();

                Data data = new Data();
                Sms sms = new Sms();

                req.rn = r.Next(Int32.MaxValue).ToString();
                Nrn = req.rn;
                req.st = "S";

                data.un = un;
                data.pwd = pwd;

                sms.txn = r.Next(Int32.MaxValue).ToString();
                Ntxn = sms.txn;

                sms.msg = msg;
                sms.to = ton;
                sms.mt = 0;
                data.sms = sms;

                string serializeddata = JsonConvert.SerializeObject(data);
                string base64Encodeddata = Convert.ToBase64String(Encoding.UTF8.GetBytes(serializeddata));
                req.data = base64Encodeddata;

                string serializedreq = JsonConvert.SerializeObject(req);
                string Method = "sendsms";
                byte[] resultbyte = wc.UploadData(api + Method, "POST", Encoding.UTF8.GetBytes(serializedreq));
                string result = Encoding.Default.GetString(resultbyte);

                deserializedResSms = JsonConvert.DeserializeObject<ResSms>(result);
                MErr = "";
                return deserializedResSms;
            }
            catch (Exception ex)
            {
                int a = ObjCls.OnSaveSMSRequest(msg, ton, "OnEveryPunch", empcode, PunchTime);
                ClsGlobal.WriteErrorLog("SendSMS : " + ex.Message);
                MErr = ex.Message.ToString();
                return deserializedResSms;
            }
        }
    }
}
