using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Data;
using System.Diagnostics;
//using System.Configuration;
using Microsoft.Practices.EnterpriseLibrary;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common;
using Newtonsoft.Json;
using System.Net;
using MySql.Data.MySqlClient;
using Newtonsoft.Json.Linq;

namespace SaaSPushTech
{
    public class ClsDALTransaction
    {

        #region "Declaration"

        private Database db;
        private DbCommand dbCmd;
        private DataSet ds;
        private DbConnection cn;
        DbTransaction tran;
        string WebApiUrl = System.Configuration.ConfigurationManager.AppSettings["WebApiUrl"].ToString();
        #endregion

        #region "Database Functions"
        //public string WebApiOnSave(ClsBALTblTransaction ObjClsBALTblTransaction)
        //{
        //    string url = WebApiUrl;
        //    WebClient WC = new WebClient();
        //    WC.Headers["Content-type"] = "application/json";
        //    WC.Headers[HttpRequestHeader.Authorization] = token;
        //    var serializedProduct = JsonConvert.SerializeObject(ObjClsBALTblTransaction);
        //    byte[] res = WC.UploadData(url + "/Transaction/tmpDmpTerminalDataDevice", "POST", Encoding.Default.GetBytes(serializedProduct));
        //    string result = System.Text.Encoding.UTF8.GetString(res);
        //    return result;

        //}
        public class MRespo
        {
            public string MegSts { get; set; }
            public string Meg { get; set; }
        }

        public class Response
        {
            public string MsgSts { get; set; }
            public string Msg { get; set; }
        }
        public class PunchData
        {
            public int tmpDmpid { get; set; }
            public int cmpid { get; set; }
            public int branchid { get; set; }
            public int[] empidlst { get; set; }
            public string[] mode { get; set; }
            public string[] In_Out_Time { get; set; }
            public string fromdate { get; set; }
            public string todate { get; set; }
            public int deviceId { get; set; }
            public string IPaddress { get; set; }
            public int EntryMode { get; set; }
            public int Punchid { get; set; }
            // public int tmpDmpid { get; set; }
            public Int64 Empid { get; set; }
            public string Empcode { get; set; }
            public string EmpName { get; set; }
            public int ShiftCode { get; set; }
            public string Attn_Dt { get; set; }
            public bool IsActive { get; set; }
            public string token { get; set; }
            public string DeviceSerialNo { get; set; }

        }

        public class DeviceSayHello
        {
            public string SerialNo { get; set; }
            public string TimeSpan { get; set; }
        }

        public string WebApiOnSave(ClsBALTblTransaction ObjClsBALTblTransaction)
        {
            string resulttmp = string.Empty;
            try
            {
                string url = WebApiUrl;
                WebClient WC = new WebClient();

                string srno = ObjClsBALTblTransaction.DeviceSerialNo;
                //WC.Headers["Content-type"] = "application/json";
                //byte[] res = WC.UploadData(url + "/Transaction/GetUserFromSerialNo?deviceSerialNo=" + srno.Trim(), "POST", new byte[] { });
                //if (res != null)
                //{
                //    string result = System.Text.Encoding.UTF8.GetString(res);
                //    MRespo resp = new MRespo();
                //    resp = JsonConvert.DeserializeObject<MRespo>(result);
                //    result = string.Empty;
                //    if (resp.MegSts.ToLower() == "ok")
                //    {
                //        string resulttmp = string.Empty;
                //        string token=string.Empty;
                //        if (!string.IsNullOrEmpty(resp.Meg.ToString()))
                //        {
                //            token = resp.Meg.ToString();
                //            PunchData PD = new PunchData();
                //            PD.token = token;
                //            PD.Attn_Dt = ObjClsBALTblTransaction.PunchTime.ToString("yyyy-MM-dd");
                //            PD.In_Out_Time = new string[] { ObjClsBALTblTransaction.PunchTime.ToString("yyyy-MM-dd HH:mm:ss") };
                //            PD.mode = new string[] { ObjClsBALTblTransaction.InOutMode };
                //            PD.IPaddress = ObjClsBALTblTransaction.DeviceIP;
                //            PD.cmpid = 0;
                //            PD.branchid = 0;
                //            PD.deviceId = Convert.ToInt32(ObjClsBALTblTransaction.DeviceID);
                //            PD.EntryMode = 1;
                //            PD.Punchid = Convert.ToInt32(ObjClsBALTblTransaction.EnrollNo);
                //            PD.cmpid = 0;
                //            PD.branchid = 0;
                //            PD.fromdate = ObjClsBALTblTransaction.PunchTime.ToString("yyyy-MM-dd");
                //            PD.todate = ObjClsBALTblTransaction.PunchTime.ToString("yyyy-MM-dd");
                //            PD.Empid = 0;
                //            PD.ShiftCode = 0;
                //            PD.IsActive = false;
                //            //api call for tmpdmpterminal for client device
                //            WebClient WC1 = new WebClient();
                //            WC1.Headers["Content-type"] = "application/json";
                //            //WC1.Headers[HttpRequestHeader.Authorization] = token;

                //            var json = JsonConvert.SerializeObject(PD);
                //            //var serializedProduct1 = JsonConvert.SerializeObject(ObjClsBALTblTransaction);
                //            //byte[] restmp = WC1.UploadData(url + "/Transaction/AddpunchingMachine", "POST", Encoding.Default.GetBytes(serializedProduct1));
                //            byte[] restmp = WC1.UploadData(url + "/Transaction/tmpDmpTerminalCreateDevice", "POST", Encoding.Default.GetBytes(json));
                //            resulttmp = System.Text.Encoding.UTF8.GetString(restmp);

                //        }
                //        return resulttmp;
                //    }
                //else
                //{
                //    return "Error";
                //}

                PunchData PD = new PunchData();
                PD.token = "";
                PD.Attn_Dt = ObjClsBALTblTransaction.PunchTime.ToString("yyyy-MM-dd");
                PD.In_Out_Time = new string[] { ObjClsBALTblTransaction.PunchTime.ToString("yyyy-MM-dd HH:mm:ss") };
                PD.mode = new string[] { ObjClsBALTblTransaction.InOutMode };
                PD.IPaddress = ObjClsBALTblTransaction.DeviceIP;
                PD.cmpid = 0;
                PD.branchid = 0;
                PD.deviceId = Convert.ToInt32(ObjClsBALTblTransaction.DeviceID);
                PD.EntryMode = 1;
                PD.Punchid = Convert.ToInt32(ObjClsBALTblTransaction.EnrollNo);
                PD.cmpid = 0;
                PD.branchid = 0;
                PD.fromdate = ObjClsBALTblTransaction.PunchTime.ToString("yyyy-MM-dd");
                PD.todate = ObjClsBALTblTransaction.PunchTime.ToString("yyyy-MM-dd");
                PD.Empid = 0;
                PD.ShiftCode = 0;
                PD.IsActive = false;
                PD.DeviceSerialNo = srno.Trim();
                //api call for tmpdmpterminal for client device
                WebClient WC1 = new WebClient();
                WC1.Headers["Content-type"] = "application/json";
                //WC1.Headers[HttpRequestHeader.Authorization] = token;

                var json = JsonConvert.SerializeObject(PD);
                //var serializedProduct1 = JsonConvert.SerializeObject(ObjClsBALTblTransaction);
                //byte[] restmp = WC1.UploadData(url + "/Transaction/AddpunchingMachine", "POST", Encoding.Default.GetBytes(serializedProduct1));
                byte[] restmp = WC1.UploadData(url + "/Transaction/tmpDmpTerminalCreateDevice", "POST", Encoding.Default.GetBytes(json));
                resulttmp = System.Text.Encoding.UTF8.GetString(restmp);
            }
            catch (Exception ex)
            {
                ClsGlobal.WriteDeveloperErrorLog("Punch Data exception: " + ex.Message + ObjClsBALTblTransaction.DeviceSerialNo);
            }

            return resulttmp;

        }


        public string WebApiOnSaveDeveloper(ClsBALTblTransaction ObjClsBALTblTransaction)
        {
            string resulttmp = string.Empty;
            try
            {
                string url = WebApiUrl;
                //WebClient WC = new WebClient();
                string srno = ObjClsBALTblTransaction.DeviceSerialNo;
                //WC.Headers["Content-type"] = "application/json";
                //byte[] res = WC.UploadData(url + "/Transaction/GetUserFromSerialNoDeveloper?deviceSerialNo=" + srno.Trim(), "POST", new byte[] { });

                //string result = System.Text.Encoding.UTF8.GetString(res);
                MRespo resp = new MRespo();
                //resp = JsonConvert.DeserializeObject<MRespo>(result);
                //result = string.Empty;
                //if (resp.MegSts.ToLower() == "ok")
                //{
                //    string resulttmp = string.Empty;
                //    string token = string.Empty;
                //    if (!string.IsNullOrEmpty(resp.Meg.ToString()))
                //    {
                //token = resp.Meg.ToString();

                PunchData PD = new PunchData();
                PD.token = string.Empty;
                PD.Attn_Dt = ObjClsBALTblTransaction.PunchTime.ToString("yyyy-MM-dd");
                PD.In_Out_Time = new string[] { ObjClsBALTblTransaction.PunchTime.ToString("yyyy-MM-dd HH:mm:ss") };
                PD.mode = new string[] { ObjClsBALTblTransaction.InOutMode };
                PD.IPaddress = ObjClsBALTblTransaction.DeviceIP;
                PD.cmpid = 0;
                PD.branchid = 0;
                PD.deviceId = Convert.ToInt32(ObjClsBALTblTransaction.DeviceID);
                PD.EntryMode = 1;
                PD.Punchid = Convert.ToInt32(ObjClsBALTblTransaction.EnrollNo);
                PD.cmpid = 0;
                PD.branchid = 0;
                PD.fromdate = ObjClsBALTblTransaction.PunchTime.ToString("yyyy-MM-dd");
                PD.todate = ObjClsBALTblTransaction.PunchTime.ToString("yyyy-MM-dd");
                PD.Empid = 0;
                PD.ShiftCode = 0;
                PD.IsActive = false;
                PD.DeviceSerialNo = srno;
                //api call for tmpdmpterminal for developer device
                WebClient WC1 = new WebClient();
                WC1 = new WebClient();
                WC1.Headers["Content-type"] = "application/json";
                //WC1.Headers[HttpRequestHeader.Authorization] = token;
                var json = JsonConvert.SerializeObject(PD);
                byte[] restmp = WC1.UploadData(url + "/Transaction/TrancationDataDeveloper", "POST", Encoding.Default.GetBytes(json));
                resulttmp = System.Text.Encoding.UTF8.GetString(restmp);
                Response resp1 = new Response();
                resp1 = JsonConvert.DeserializeObject<Response>(resulttmp);
                if (resp1.MsgSts.ToLower() == "ok")
                {
                    ClsGlobal.WriteDeveloperSucessLog("Developer Punch Data: " + srno + "  " + json);
                }
                else
                {
                    ClsGlobal.WriteDeveloperErrorLog("Developer Punch Data Error: " + resp1.Msg + " " + srno + "  " + json);
                }
                //developer find end point 

                //WC1 = new WebClient();
                //WC1.Headers["Content-type"] = "application/text";
                ////WC1.Headers[HttpRequestHeader.Authorization] = token;
                ////WC.ContentType = "application/text";
                //byte[] re1 = WC1.UploadData(url + "/Transaction/GetDeveloperEndPoint", "POST", new byte[] { });
                //string result2 = System.Text.Encoding.UTF8.GetString(re1);
                //DataTable dt1 = (DataTable)JsonConvert.DeserializeObject(result2, (typeof(DataTable)));

                //if (dt1 != null && dt1.Rows.Count > 0)
                //{
                //    string endpointtrancationdata = dt1.Rows[0][2].ToString();
                //    DeviceSayHello drboj = new DeviceSayHello();
                //    //var json = JsonConvert.SerializeObject(drboj);
                //    WC1 = new WebClient();
                //    WC1.Headers["Content-type"] = "application/json";
                //    byte[] re2 = WC1.UploadData(endpointtrancationdata, "POST", Encoding.Default.GetBytes(json));
                //    string reslt = System.Text.Encoding.UTF8.GetString(re2);
                //    resp = new MRespo();
                //    resp = JsonConvert.DeserializeObject<MRespo>(reslt);
                //    if (resp.MegSts.ToLower() == "ok")
                //    {
                //        ClsGlobal.WriteDeveloperSucessLog("Developer Punch Data: " + srno + "  " + json);
                //    }
                //    else
                //    {
                //        ClsGlobal.WriteDeveloperErrorLog("Developer Punch Data: " + srno + "  " + json);
                //    }
                //}

            }

            catch (Exception ex)
            {
                ClsGlobal.WriteDeveloperErrorLog("Developer Punch Data exception: " + ex.Message + ObjClsBALTblTransaction.DeviceSerialNo);
            }
            //return "";
            return resulttmp;
        }
        //public string WebApiOnSayHello(string DeviceSrNo)
        //{
        //    string url = WebApiUrl;
        //    WebClient WC = new WebClient();
        //    WC.Headers["Content-type"] = "application/text";
        //    //WC.ContentType = "application/text";
        //    byte[] res = WC.UploadData(url + "/Transaction/SayHello?DeviceSrNo=" + DeviceSrNo, "POST", new byte[] { });
        //    string result = System.Text.Encoding.UTF8.GetString(res);
        //    return result;

        //}
        //public string WebApiOnSayHello(string DeviceSrNo)
        //{
        //    string result1 = string.Empty;

        //    try
        //    {

        //        string url = WebApiUrl;
        //        WebClient WC = new WebClient();
        //        string srno = DeviceSrNo;
        //        WC.Headers["Content-type"] = "application/json";
        //        byte[] res = WC.UploadData(url + "/Transaction/GetUserFromSerialNo?deviceSerialNo=" + srno, "POST", new byte[] { });
        //        if (res != null)
        //        {
        //            string result = System.Text.Encoding.UTF8.GetString(res);
        //            MRespo resp = new MRespo();
        //            resp = JsonConvert.DeserializeObject<MRespo>(result);
        //            if (resp.MegSts.ToLower() == "ok")
        //            {
        //                string resulttmp = string.Empty;
        //                if (!string.IsNullOrEmpty(resp.Meg.ToString()))
        //                {
        //                    string token = resp.Meg.ToString();
        //                    WebClient WC1 = new WebClient();
        //                    WC1.Headers["Content-type"] = "application/text";
        //                    WC1.Headers[HttpRequestHeader.Authorization] = token;
        //                    //WC.ContentType = "application/text";
        //                    byte[] res1 = WC1.UploadData(url + "/Transaction/SayHello?DeviceSrNo=" + DeviceSrNo, "POST", new byte[] { });
        //                    result1 = System.Text.Encoding.UTF8.GetString(res);

        //                }
        //            }

        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ClsGlobal.WriteErrorLog("WebApiOnSayHello: " + ex.Message + DeviceSrNo);
        //    }
        //    return result1;

        //}

        public string WebApiOnSayHello(string DeviceSrNo)
        {
            string result = string.Empty;

            try
            {

                string url = WebApiUrl;
                WebClient WC = new WebClient();
                string srno = DeviceSrNo;
                WC.Headers["Content-type"] = "application/json";
                byte[] res = WC.UploadData(url + "/Transaction/GetUserFromSerialNo?deviceSerialNo=" + srno.Trim(), "POST", new byte[] { });
                result = System.Text.Encoding.UTF8.GetString(res);
                //MRespo resp = new MRespo();
                //resp = JsonConvert.DeserializeObject<MRespo>(result);

                //if (res != null)
                //{
                //    string result = System.Text.Encoding.UTF8.GetString(res);
                //    MRespo resp = new MRespo();
                //    resp = JsonConvert.DeserializeObject<MRespo>(result);
                //    if (resp.MegSts.ToLower() == "ok")
                //    {
                //        string resulttmp = string.Empty;
                //        if (!string.IsNullOrEmpty(resp.Meg.ToString()))
                //        {
                //            string token = resp.Meg.ToString();
                //            WebClient WC1 = new WebClient();
                //            WC1.Headers["Content-type"] = "application/text";
                //            WC1.Headers[HttpRequestHeader.Authorization] = token;
                //            //WC.ContentType = "application/text";
                //            byte[] res1 = WC1.UploadData(url + "/Transaction/SayHello?DeviceSrNo=" + DeviceSrNo, "POST", new byte[] { });
                //            result1 = System.Text.Encoding.UTF8.GetString(res);

                //        }
                //    }

                //}
            }
            catch (Exception ex)
            {
                ClsGlobal.WriteErrorLog("WebApiOnSayHello: " + ex.Message + DeviceSrNo);
            }
            return result;

        }
        public string WebApiOnSayHelloDeveloper(string DeviceSrNo)
        {
            string result1 = string.Empty;

            try
            {

                string url = WebApiUrl;
                WebClient WC = new WebClient();
                string srno = DeviceSrNo;
                WC.Headers["Content-type"] = "application/json";
                byte[] res = WC.UploadData(url + "/Transaction/GetUserFromSerialNoDeveloper?deviceSerialNo=" + srno.Trim(), "POST", new byte[] { });
                result1 = System.Text.Encoding.UTF8.GetString(res);

                //string url = WebApiUrl;
                //WebClient WC = new WebClient();
                //string srno = DeviceSrNo;
                //WC.Headers["Content-type"] = "application/json";
                //byte[] res = WC.UploadData(url + "/Transaction/GetUserFromSerialNoDeveloper?deviceSerialNo=" + srno, "POST", new byte[] { });
                //if (res != null)
                //{
                //    string result = System.Text.Encoding.UTF8.GetString(res);
                //    MRespo resp = new MRespo();
                //    resp = JsonConvert.DeserializeObject<MRespo>(result);
                //    if (resp.MegSts.ToLower() == "ok")
                //    {
                //        string resulttmp = string.Empty;
                //        if (!string.IsNullOrEmpty(resp.Meg.ToString()))
                //        {
                //            string token = resp.Meg.ToString();
                //            WebClient WC1 = new WebClient();
                //            WC1.Headers["Content-type"] = "application/text";
                //            WC1.Headers[HttpRequestHeader.Authorization] = token;
                //            //WC.ContentType = "application/text";
                //            byte[] res1 = WC1.UploadData(url + "/Transaction/SayHello?DeviceSrNo=" + DeviceSrNo, "POST", new byte[] { });
                //            result1 = System.Text.Encoding.UTF8.GetString(res);

                //            //developer find end point 

                //            WC1 = new WebClient();
                //            WC1.Headers["Content-type"] = "application/text";
                //            WC1.Headers[HttpRequestHeader.Authorization] = token;
                //            //WC.ContentType = "application/text";
                //            byte[] re1 = WC1.UploadData(url + "/Transaction/GetDeveloperEndPoint", "POST", new byte[] { });
                //            string result2 = System.Text.Encoding.UTF8.GetString(re1);
                //            DataTable dt1 = (DataTable)JsonConvert.DeserializeObject(result2, (typeof(DataTable)));

                //            if (dt1 != null && dt1.Rows.Count > 0)
                //            {
                //                string endpointsayhello = dt1.Rows[0][1].ToString();
                //                DeviceSayHello drboj = new DeviceSayHello();
                //                drboj.SerialNo = srno;
                //                drboj.TimeSpan = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");
                //                var json = JsonConvert.SerializeObject(drboj);
                //                WC1 = new WebClient();
                //                WC1.Headers["Content-type"] = "application/json";
                //                byte[] re2 = WC1.UploadData(endpointsayhello, "POST", Encoding.Default.GetBytes(json));
                //                string reslt = System.Text.Encoding.UTF8.GetString(re2);
                //                resp = new MRespo();
                //                resp = JsonConvert.DeserializeObject<MRespo>(reslt);
                //                if (resp.MegSts.ToLower() == "ok")
                //                {
                //                    ClsGlobal.WriteDeveloperSucessLog("sayhello: " + srno + "  " + json);
                //                }
                //                else
                //                {
                //                    ClsGlobal.WriteDeveloperErrorLog("sayhello: " + srno + "  " + json);
                //                }
                //            }


                //        }
                //    }

                //}
            }
            catch (Exception ex)
            {
                ClsGlobal.WriteErrorLog("WebApiOnSayHello: " + ex.Message + DeviceSrNo);
            }
            return result1;

        }
        public int OnSave(ClsBALTblTransaction ObjClsBALTblTransaction)
        {
            try
            {
                string srno = ObjClsBALTblTransaction.DeviceSerialNo;
                WebClient WC = new WebClient();
                WC.Headers["Content-type"] = "application/json";
                byte[] res1 = WC.UploadData(WebApiUrl + "/Transaction/CheckDeviceIsDeveloper?deviceSerialNo=" + srno.Trim(), "POST", new byte[] { });

                if (res1 != null)
                {
                    string result1 = System.Text.Encoding.UTF8.GetString(res1);
                    MRespo resp1 = new MRespo();
                    resp1 = JsonConvert.DeserializeObject<MRespo>(result1);

                    if (resp1.MegSts.ToLower() == "ok")
                    {
                        if (resp1.Meg == "1") //developer device
                        {
                            string res2 = WebApiOnSaveDeveloper(ObjClsBALTblTransaction);
                            Response resp2 = new Response();
                            resp2 = JsonConvert.DeserializeObject<Response>(res2.ToString());
                            if (resp2.MsgSts.ToLower() == "ok")
                            {
                                return 1;
                            }
                            else
                            {
                                return -2;
                            }
                        }
                        else if (resp1.Meg == "2") //school device
                        {
                            string res2 = WebApiOnSaveSchool(ObjClsBALTblTransaction);
                            Response resp2 = new Response();
                            resp2 = JsonConvert.DeserializeObject<Response>(res2.ToString());
                            if (resp2.MsgSts.ToLower() == "ok")
                            {
                                return 1;
                            }
                            else
                            {
                                return -2;
                            }
                        }
                        else if (resp1.Meg == "0") //client device
                        {
                            string res = WebApiOnSave(ObjClsBALTblTransaction);
                            MRespo resp = new MRespo();
                            resp = JsonConvert.DeserializeObject<MRespo>(res.ToString());
                            if (resp.MegSts.ToLower() == "ok")
                            {
                                if (resp.Meg.ToLower() == "na pid")
                                {
                                    return 3;
                                }
                                else
                                {
                                    return 1;
                                }
                            }
                            else
                            {
                                return -2;
                            }
                        }
                        else
                        {
                            return -2;
                        }
                    }

                    else
                    {
                        return -2;
                    }
                }

                else
                {

                    return -2;
                }

            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("IX_tmpDmpTerminalData") == true || ex.Message.Contains("The transaction ended in the trigger"))
                {
                    //tran.Rollback();
                    return 2;
                }
                else
                {
                    throw ex;
                }
            }
            //finally
            //{
            //    dbCmd.Connection.Close();
            //    dbCmd.Dispose();
            //    cn.Close();
            //}
        }

        public string WebApiOnSaveSchool(ClsBALTblTransaction ObjClsBALTblTransaction)
        {
            string resulttmp = string.Empty;
            try
            {
                string url = WebApiUrl;
                string srno = ObjClsBALTblTransaction.DeviceSerialNo;
                MRespo resp = new MRespo();
                PunchData PD = new PunchData();
                PD.token = string.Empty;
                PD.Attn_Dt = ObjClsBALTblTransaction.PunchTime.ToString("yyyy-MM-dd");
                PD.In_Out_Time = new string[] { ObjClsBALTblTransaction.PunchTime.ToString("yyyy-MM-dd HH:mm:ss") };
                PD.mode = new string[] { ObjClsBALTblTransaction.InOutMode };
                PD.IPaddress = ObjClsBALTblTransaction.DeviceIP;
                PD.cmpid = 0;
                PD.branchid = 0;
                PD.deviceId = Convert.ToInt32(ObjClsBALTblTransaction.DeviceID);
                PD.EntryMode = 1;
                PD.Punchid = Convert.ToInt32(ObjClsBALTblTransaction.EnrollNo);
                PD.cmpid = 0;
                PD.branchid = 0;
                PD.fromdate = ObjClsBALTblTransaction.PunchTime.ToString("yyyy-MM-dd");
                PD.todate = ObjClsBALTblTransaction.PunchTime.ToString("yyyy-MM-dd");
                PD.Empid = 0;
                PD.ShiftCode = 0;
                PD.IsActive = false;
                PD.DeviceSerialNo = srno;
                //api call for tmpdmpterminal for developer device
                WebClient WC1 = new WebClient();
                WC1 = new WebClient();
                WC1.Headers["Content-type"] = "application/json";
                //WC1.Headers[HttpRequestHeader.Authorization] = token;
                var json = JsonConvert.SerializeObject(PD);
                byte[] restmp = WC1.UploadData(url + "/Transaction/TrancationDataSchool", "POST", Encoding.Default.GetBytes(json));
                resulttmp = System.Text.Encoding.UTF8.GetString(restmp);
                Response resp1 = new Response();
                resp1 = JsonConvert.DeserializeObject<Response>(resulttmp);
                string sm = string.Empty;
                if (resp1.MsgSts.ToLower() == "ok")
                {
                    sm = PD.DeviceSerialNo + "," + PD.Punchid + "," + ObjClsBALTblTransaction.PunchTime.ToString("yyyy-MM-dd HH:mm:ss") + "," + PD.deviceId + "," + PD.fromdate;
                    ClsGlobal.WriteSchoolSucessLog("Student Punch Data: " + resp1.Msg + " " + srno + "  " + sm);
                }
                else
                {
                    sm = PD.DeviceSerialNo + "," + PD.Punchid + "," + ObjClsBALTblTransaction.PunchTime.ToString("yyyy-MM-dd HH:mm:ss") + "," + PD.deviceId + "," + PD.fromdate;
                    ClsGlobal.WriteSchoolErrorLog("Student Punch Data Error: " + resp1.Msg + " " + srno + "  " + sm);
                }


            }

            catch (Exception ex)
            {
                ClsGlobal.WriteDeveloperErrorLog("Developer Punch Data exception: " + ex.Message + ObjClsBALTblTransaction.DeviceSerialNo);
            }
            //return "";
            return resulttmp;
        }

        //public int OnSave(ClsBALTblTransaction ObjClsBALTblTransaction)
        //{
        //    try
        //    {
        //        //db = DatabaseFactory.CreateDatabase();
        //        //SqlDatabase Obj = new SqlDatabase(ClsGlobal.LocalConStr);
        //        //db = Obj;
        //        //cn = db.CreateConnection();
        //        //cn.Open();
        //        //tran = cn.BeginTransaction();

        //        //dbCmd = db.GetStoredProcCommand("OnSave_SP_tmpDmpTerminalData");
        //        //dbCmd.CommandTimeout = 0;
        //        //db.AddInParameter(dbCmd, "@In_Out_Time", DbType.DateTime, ObjClsBALTblTransaction.PunchTime.ToString("yyyy-MM-dd HH:mm"));
        //        //db.AddInParameter(dbCmd, "@Attn_Dt", DbType.DateTime, ObjClsBALTblTransaction.PunchTime.ToString("yyyy-MM-dd"));
        //        //db.AddInParameter(dbCmd, "@Mode", DbType.String, ObjClsBALTblTransaction.InOutMode);
        //        //db.AddInParameter(dbCmd, "@IPaddress", DbType.String, ObjClsBALTblTransaction.DeviceIP);
        //        //db.AddInParameter(dbCmd, "@DeviceID", DbType.Int32, ObjClsBALTblTransaction.DeviceID);
        //        //db.AddInParameter(dbCmd, "@Attndt", DbType.Decimal, ObjClsBALTblTransaction.PunchTime.ToString("yyyyMMdd"));
        //        //db.AddInParameter(dbCmd, "@InOutTime", DbType.Decimal, ObjClsBALTblTransaction.PunchTime.ToString("yyyyMMddHHmmss"));
        //        //db.AddInParameter(dbCmd, "@CardNo", DbType.String, ObjClsBALTblTransaction.EnrollNo);
        //        //db.ExecuteNonQuery(dbCmd);
        //        //tran.Commit();
        //        //string url = "http://localhost:2159/Api/";
        //        //WebClient WC = new WebClient();
        //        //WC.Headers["Content-type"] = "application/json";
        //        //var serializedProduct = JsonConvert.SerializeObject(ObjClsBALTblTransaction);
        //        //byte[] res = WC.UploadData(url + "/Transaction/tmpDmpTerminalDataDevice", "POST", Encoding.Default.GetBytes(serializedProduct));
        //        //string result = System.Text.Encoding.UTF8.GetString(res);
        //        string res = WebApiOnSave(ObjClsBALTblTransaction);
        //        MRespo resp = new MRespo();
        //        resp = JsonConvert.DeserializeObject<MRespo>(res.ToString());
        //        if (resp.MegSts.ToLower() == "ok")
        //        {
        //            if (resp.Meg.ToLower() == "punchid not available in employee master.")
        //            {
        //                return 3;
        //            }
        //            else
        //            {
        //                return 1;
        //            }
        //        }
        //        else
        //        {
        //            return -2;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        if (ex.Message.Contains("IX_tmpDmpTerminalData") == true || ex.Message.Contains("The transaction ended in the trigger"))
        //        {
        //            //tran.Rollback();
        //            return 2;
        //        }
        //        else
        //        {
        //            throw ex;
        //        }
        //    }
        //    //finally
        //    //{
        //    //    dbCmd.Connection.Close();
        //    //    dbCmd.Dispose();
        //    //    cn.Close();
        //    //}
        //}
        //public void SayHello(Int64 DeviceID)
        //{
        //    try
        //    {
        //        //db = DatabaseFactory.CreateDatabase();
        //        SqlDatabase Obj = new SqlDatabase(ClsGlobal.LocalConStr);
        //        db = Obj;
        //        int getresult = 0;
        //        dbCmd = db.GetStoredProcCommand("SayHello");
        //        db.AddInParameter(dbCmd, "@DeviceID", DbType.Int64, DeviceID);
        //        getresult = db.ExecuteNonQuery(dbCmd);
        //    }
        //    catch (Exception ex)
        //    {
        //        ClsGlobal.WriteErrorLog("SayHello DAL: " + ex.Message);
        //    }
        //}
        //public void SayHello(string DeviceSrNo)
        //{
        //    try
        //    {
        //        //db = DatabaseFactory.CreateDatabase();
        //        //SqlDatabase Obj = new SqlDatabase(ClsGlobal.LocalConStr);
        //        //db = Obj;
        //        //int getresult = 0;
        //        //dbCmd = db.GetStoredProcCommand("SayHello");
        //        //db.AddInParameter(dbCmd, "@DeviceID", DbType.Int64, DeviceID);
        //        //getresult = db.ExecuteNonQuery(dbCmd);
        //        //string qr = "UPDATE tbldevicemaster  SET  LastActivity = '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "' WHERE DeviceSrNo = '" + DeviceSrNo + "' ";
        //        //ExecuteNonQuery(qr);
        //        string res = WebApiOnSayHello(DeviceSrNo);
        //        MRespo resp = new MRespo();
        //        resp = JsonConvert.DeserializeObject<MRespo>(res.ToString());
        //        if (resp.MegSts == "ok")
        //        {
        //            //ClsGlobal.WriteErrorLog("SayHello DAL: " + "Sucees In SayHello:" + DeviceSrNo);
        //            ClsGlobal.WriteErrorLog("SayHello DAL: " + "Sucees In SayHello:" + DeviceSrNo);
        //        }
        //        else
        //        {
        //            ClsGlobal.WriteErrorLog("SayHello DAL: " + "Error In SayHello:" + DeviceSrNo);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ClsGlobal.WriteErrorLog("SayHello DAL: " + DeviceSrNo + ex.Message);
        //    }
        //}
        public void SayHello(string DeviceSrNo)
        {
            try
            {
                WebClient WC = new WebClient();
                WC.Headers["Content-type"] = "application/json";
                byte[] res1 = WC.UploadData(WebApiUrl + "/Transaction/CheckDeviceIsDeveloper?deviceSerialNo=" + DeviceSrNo.Trim(), "POST", new byte[] { });
                if (res1 != null)
                {
                    string result1 = System.Text.Encoding.UTF8.GetString(res1);
                    MRespo resp1 = new MRespo();
                    resp1 = JsonConvert.DeserializeObject<MRespo>(result1);
                    Response resm = new Response();
                    string res = string.Empty;
                    MRespo resp;
                    if (resp1.MegSts.ToLower() == "ok")
                    {
                        if (resp1.Meg == "1") // developer device
                        {

                            res = WebApiOnSayHelloDeveloper(DeviceSrNo);

                            resm = new Response();
                            resm = JsonConvert.DeserializeObject<Response>(res.ToString());
                            if (resm.MsgSts == "ok")
                            {
                                string meg = "Developer" + " " + resm.Msg + " " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:dd");
                                ClsGlobal.WriteSayHello(meg);
                            }
                            else
                            {
                                //ClsGlobal.WriteErrorLog("SayHello DAL Dev: " + "Error In SayHello:" + DeviceSrNo);
                                string meg = "Developer error" + resm.Msg;
                                ClsGlobal.WriteSayHello(meg);
                            }
                        }
                        else if (resp1.Meg == "2") // school device
                        {

                            res = WebApiOnSayHello(DeviceSrNo);
                            resp = new MRespo();
                            resp = JsonConvert.DeserializeObject<MRespo>(res.ToString());
                            if (resp.MegSts == "ok")
                            {
                                ClsGlobal.WriteSayHello("School" + " "  + resp.Meg + " " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:dd"));
                            }
                            else
                            {
                                //ClsGlobal.WriteSayHello(meg);
                                ClsGlobal.WriteSayHello("School error in sayhello:" + resp.Meg + " " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:dd"));
                            }
                        }
                        else if (resp1.Meg == "0") // client device
                        {

                            res = WebApiOnSayHello(DeviceSrNo);
                            resp = new MRespo();
                            resp = JsonConvert.DeserializeObject<MRespo>(res.ToString());
                            if (resp.MegSts == "ok")
                            {
                                //ClsGlobal.WriteSayHello(resp.Meg);
                                ClsGlobal.WriteSayHello("Client" + "  " + resp.Meg + " " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:dd"));
                            }
                            else
                            {
                                //ClsGlobal.WriteSayHello(resp.Meg);
                                ClsGlobal.WriteSayHello("Client error in sayhello:" + resp.Meg + " " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:dd"));
                            }

                        }
                    }
                    else
                    {
                        ClsGlobal.WriteErrorLog("Get error from CheckDeviceIsDeveloper Api.");

                    }

                }
                else
                {
                    ClsGlobal.WriteErrorLog("Error to get responce from CheckDeviceIsDeveloper Api");
                }




            }
            catch (Exception ex)
            {
                ClsGlobal.WriteErrorLog("SayHello DAL: " + DeviceSrNo + ex.Message);
            }
        }
        
        //public DataSet GetServerPort(ClsBALTblTransaction ObjClsBALTblTransaction)
        //{
        //    try
        //    {
        //        //db = DatabaseFactory.CreateDatabase();
        //        SqlDatabase Obj = new SqlDatabase(ClsGlobal.LocalConStr);
        //        db = Obj;
        //        dbCmd = db.GetStoredProcCommand("Get_ServerPort");
        //        dbCmd.CommandTimeout = 0;

        //        ds = new DataSet();
        //        ds = db.ExecuteDataSet(dbCmd);
        //        return ds;
        //    }
        //    catch (Exception ex)
        //    {
        //        ClsGlobal.WriteErrorLog("GetServerPort DAL : " + ex.Message);
        //        return null;
        //    }

        //}
        //public DataTable GetServerPort(ClsBALTblTransaction ObjClsBALTblTransaction)
        //{
        //    try
        //    {
        //        //string str = "select * from ( Select ifnull(ServerPort,7005) as ServerPort,DeviceTypeCode from tblDeviceTypeMaster where DeviceTypeCode = 10  Union  ";
        //        //str = str + " Select ifnull(ServerPort,5005) as ServerPort,DeviceTypeCode from tblDeviceTypeMaster where DeviceTypeCode = 12 Union ";
        //        //str = str + " Select ifnull(ServerPort,2000) as ServerPort,DeviceTypeCode from tblDeviceTypeMaster where DeviceTypeCode = 28 Union";
        //        //str = str + " Select ifnull(ServerPort,7005) as ServerPort,DeviceTypeCode from tblDeviceTypeMaster where DeviceTypeCode = 3  ) as A order by DeviceTypeCode";
        //        //DataSet ds = GetTable(str);
        //        string json = WebApiGetServerPort();
        //        DataTable dm = (DataTable)JsonConvert.DeserializeObject(json, (typeof(DataTable)));
        //        //DataSet dt = (DataSet)JsonConvert.DeserializeObject(json, (typeof(DataSet)));
        //        return dm;
        //    }
        //    catch (Exception ex)
        //    {
        //        ClsGlobal.WriteErrorLog("GetServerPort DAL : " + ex.Message);
        //        return null;

        //    }
        //}

        public DataSet GetAllEmpSMS(Int64 EmpID)
        {
            try
            {
                //db = DatabaseFactory.CreateDatabase();
                SqlDatabase Obj = new SqlDatabase(ClsGlobal.LocalConStr);
                db = Obj;
                dbCmd = db.GetStoredProcCommand("GetAll_SP_PushDataForSMS");
                dbCmd.CommandTimeout = 0;
                db.AddInParameter(dbCmd, "EmpID", DbType.Int64, EmpID);
                ds = new DataSet();
                ds = db.ExecuteDataSet(dbCmd);
                return ds;
            }
            catch (Exception ex)
            {
                ClsGlobal.WriteErrorLog("GetAllEmpSMS : " + ex.Message);
                return null;
            }
        }
        public DataSet CheckSMSSent(Int64 EmpID, DateTime time)
        {
            try
            {
                //db = DatabaseFactory.CreateDatabase();
                SqlDatabase Obj = new SqlDatabase(ClsGlobal.LocalConStr);
                db = Obj;
                dbCmd = db.GetStoredProcCommand("CheckSMSSent_SP_OnEveryPunch");
                dbCmd.CommandTimeout = 0;
                db.AddInParameter(dbCmd, "EmpID", DbType.Int64, EmpID);
                db.AddInParameter(dbCmd, "In_Out_Time", DbType.DateTime, time);
                ds = new DataSet();
                ds = db.ExecuteDataSet(dbCmd);
                return ds;
            }
            catch (Exception ex)
            {
                ClsGlobal.WriteErrorLog("CheckSMSSent : " + ex.Message);
                return null;
            }
        }
        public int OnSaveSMSLog(string EventType, int EmpCode, int Status, string Msg, string ErrCode, string ErrMsg, string Txn)
        {
            try
            {
                int result = 0;
                //db = DatabaseFactory.CreateDatabase();
                SqlDatabase Obj = new SqlDatabase(ClsGlobal.LocalConStr);
                db = Obj;

                dbCmd = db.GetStoredProcCommand("OnSave_SMSLog");
                dbCmd.CommandTimeout = 0;
                db.AddInParameter(dbCmd, "EventType", DbType.String, EventType);
                db.AddInParameter(dbCmd, "EmpCode", DbType.Int32, EmpCode);
                db.AddInParameter(dbCmd, "Status", DbType.Int32, Status);
                db.AddInParameter(dbCmd, "Msg", DbType.String, Msg);
                db.AddInParameter(dbCmd, "ErrCode", DbType.String, ErrCode);
                db.AddInParameter(dbCmd, "ErrMsg", DbType.String, ErrMsg);
                db.AddInParameter(dbCmd, "Txn", DbType.String, Txn);
                db.AddOutParameter(dbCmd, "RetVal", DbType.String, 100);
                int a = db.ExecuteNonQuery(dbCmd);
                return result = Convert.ToInt32(db.GetParameterValue(dbCmd, "RetVal"));
            }
            catch (Exception ex)
            {
                ClsGlobal.WriteErrorLog("OnSaveSMSLog " + ex.Message);
                return 0;
            }
        }

        public int OnSaveOnEveryPunch(int EmpCode, DateTime MIN_OUT_Time)
        {
            try
            {
                //db = DatabaseFactory.CreateDatabase();
                SqlDatabase Obj = new SqlDatabase(ClsGlobal.LocalConStr);
                db = Obj;

                dbCmd = db.GetStoredProcCommand("OnSave_SP_OnEveryPunch");
                dbCmd.CommandTimeout = 0;
                db.AddInParameter(dbCmd, "EmpCode", DbType.Int32, EmpCode);
                db.AddInParameter(dbCmd, "MIN_OUT_Time", DbType.DateTime, MIN_OUT_Time);
                int a = db.ExecuteNonQuery(dbCmd);
                return a;
            }
            catch (Exception ex)
            {
                ClsGlobal.WriteErrorLog("OnSaveOnEveryPunch " + ex.Message);
                return 0;
            }
        }
        public int OnDeleteOnEveryPunch(int EmpCode, String MIN_OUT_Time)
        {
            try
            {
                //db = DatabaseFactory.CreateDatabase();
                SqlDatabase Obj = new SqlDatabase(ClsGlobal.LocalConStr);
                db = Obj;

                dbCmd = db.GetStoredProcCommand("OnDelete_SP_OnEveryPunch");
                dbCmd.CommandTimeout = 0;
                db.AddInParameter(dbCmd, "EmpCode", DbType.Int32, EmpCode);
                db.AddInParameter(dbCmd, "MIN_OUT_Time", DbType.String, MIN_OUT_Time);
                int a = db.ExecuteNonQuery(dbCmd);
                return a;
            }
            catch (Exception ex)
            {
                ClsGlobal.WriteErrorLog("OnDeleteOnEveryPunch " + ex.Message);
                return 0;
            }
        }
        public int OnSaveSMSRequest(string Msg, string tos, string Event, Int32 EmpCode, DateTime PunchTime)
        {
            try
            {
                int result = 0;
                //db = DatabaseFactory.CreateDatabase();
                SqlDatabase Obj = new SqlDatabase(ClsGlobal.LocalConStr);
                db = Obj;

                dbCmd = db.GetStoredProcCommand("OnSave_TblSMSRequest");
                dbCmd.CommandTimeout = 0;
                db.AddInParameter(dbCmd, "Msg", DbType.String, Msg);
                db.AddInParameter(dbCmd, "tos", DbType.String, tos);
                db.AddInParameter(dbCmd, "Event", DbType.String, Event);
                db.AddInParameter(dbCmd, "EmpCode", DbType.Int32, EmpCode);
                db.AddInParameter(dbCmd, "PunchTime", DbType.DateTime, PunchTime);
                db.AddOutParameter(dbCmd, "RetVal", DbType.String, 100);
                int a = db.ExecuteNonQuery(dbCmd);
                return result = Convert.ToInt32(db.GetParameterValue(dbCmd, "RetVal"));
            }
            catch (Exception ex)
            {
                ClsGlobal.WriteErrorLog("OnSaveSMSRequest " + ex.Message);
                return 0;
            }
        }
        //public int OnSaveFail(ClsBALTblTransaction ObjClsBALTblTransaction)
        //{
        //    try
        //    {
        //        db = DatabaseFactory.CreateDatabase();
        //        cn = db.CreateConnection();
        //        cn.Open();
        //        tran = cn.BeginTransaction();
        //        int i = 0;

        //        dbCmd = db.GetStoredProcCommand("OnSave_SP_TblFailTransaction");
        //        dbCmd.CommandTimeout = 0;
        //        db.AddInParameter(dbCmd, "@EnrollNo", DbType.Int64, ObjClsBALTblTransaction.EnrollNo);
        //        db.AddInParameter(dbCmd, "@PunchTime", DbType.DateTime, ObjClsBALTblTransaction.PunchTime.ToString("yyyy-MM-dd HH:mm:ss"));
        //        db.AddInParameter(dbCmd, "@VerifyMode", DbType.String, ObjClsBALTblTransaction.VerifyMode);
        //        db.AddInParameter(dbCmd, "@InOutMode", DbType.String, ObjClsBALTblTransaction.InOutMode);
        //        db.AddInParameter(dbCmd, "@DeviceIP", DbType.String, ObjClsBALTblTransaction.DeviceIP);
        //        db.AddInParameter(dbCmd, "@DevicePort", DbType.Int64, ObjClsBALTblTransaction.DeviceProt);
        //        db.AddInParameter(dbCmd, "@DeviceID", DbType.String, ObjClsBALTblTransaction.DeviceID);
        //        db.AddInParameter(dbCmd, "@ErrorCode", DbType.Int64, ObjClsBALTblTransaction.ErrorCode);
        //        db.AddInParameter(dbCmd, "@ErrorDesc", DbType.String, ObjClsBALTblTransaction.ErrorDesc);

        //        i = db.ExecuteNonQuery(dbCmd);
        //        tran.Commit();
        //        return i;
        //    }
        //    catch (Exception ex)
        //    {
        //        ClsGlobal.WriteErrorLog("OnSaveFail DAL " + ex.Message);
        //        return -1;
        //    }
        //    finally
        //    {
        //        dbCmd.Connection.Close();
        //        dbCmd.Dispose();
        //        cn.Close();
        //    }
        //}
        //public DataSet GetAll(ClsBALTblTransaction ObjClsBALTblTransaction)
        //{
        //    try
        //    {
        //        db = DatabaseFactory.CreateDatabase();
        //        dbCmd = db.GetStoredProcCommand("sp_SelectData");
        //        dbCmd.CommandTimeout = 0;

        //        ds = new DataSet();
        //        ds = db.ExecuteDataSet(dbCmd);
        //        return ds;
        //    }
        //    catch (Exception ex)
        //    {
        //        ClsGlobal.WriteErrorLog("GetAll DAL : " + ex.Message);
        //        return null;
        //    }
        //}
        //public void UpdateTransaction(DataTable dt)
        //{
        //    try
        //    {
        //        db = DatabaseFactory.CreateDatabase();
        //        for (int i = 0; i < dt.Rows.Count; i++)
        //        {
        //            int getresult = 0;
        //            dbCmd = db.GetStoredProcCommand("Update_TblTransaction");
        //            db.AddInParameter(dbCmd, "@EnrollNo", DbType.Int32, Convert.ToInt32(dt.Rows[i]["EnrollNo"]));
        //            db.AddInParameter(dbCmd, "@PunchTime", DbType.DateTime, Convert.ToDateTime(dt.Rows[i]["PunchTime"]));
        //            db.AddInParameter(dbCmd, "@DeviceID", DbType.Int32, Convert.ToInt32(dt.Rows[i]["DeviceID"]));
        //            db.AddInParameter(dbCmd, "@StatusFlag", DbType.Int32, Convert.ToInt32(dt.Rows[i]["StatusFlag"]));
        //            db.AddInParameter(dbCmd, "@ErrorCode", DbType.Int32, Convert.ToInt32(dt.Rows[i]["ErrorCode"]));
        //            db.AddInParameter(dbCmd, "@ErrorMsg", DbType.String, Convert.ToString(dt.Rows[i]["ErrorMsg"]));
        //            getresult = db.ExecuteNonQuery(dbCmd);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ClsGlobal.WriteErrorLog("UpdateTransaction DAL: " + ex.Message);
        //    }
        //}



        public DataSet GetTable(string query)
        {
            DataSet objDataTable = new DataSet();
            MySqlDataAdapter objMySqlDataAdapter = new MySqlDataAdapter();
            MySqlConnection tmpConn = new MySqlConnection(ClsGlobal.LocalConStrMySql);
            MySqlCommand comm = new MySqlCommand(query);
            try
            {
                tmpConn.Open();
                comm.Connection = tmpConn;
                comm.CommandType = CommandType.Text;
                objMySqlDataAdapter.SelectCommand = comm;
                objMySqlDataAdapter.Fill(objDataTable);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                tmpConn.Close();
            }
            return objDataTable;
        }

        public int ExecuteNonQuery(string query)
        {
            int n = -1;
            MySqlConnection tmpConn = new MySqlConnection(ClsGlobal.LocalConStrMySql);
            try
            {
                tmpConn.Open();
                MySqlCommand comm = new MySqlCommand(query, tmpConn);
                n = comm.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                tmpConn.Close();
            }

            return n;
        }

        #endregion
    }
}
