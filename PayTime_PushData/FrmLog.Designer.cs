namespace SaaSPushTech
{
    partial class FrmLog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmLog));
            this.lblTotal = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ShowtoolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.HidetoolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ExittoolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.pnlTop = new System.Windows.Forms.Panel();
            this.Label1 = new System.Windows.Forms.Label();
            this.cmdCloseNetWork = new System.Windows.Forms.Button();
            this.cmdOpenNetWork = new System.Windows.Forms.Button();
            this.contextMenuStrip1.SuspendLayout();
            this.pnlTop.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTotal
            // 
            this.lblTotal.AutoSize = true;
            this.lblTotal.BackColor = System.Drawing.SystemColors.Control;
            this.lblTotal.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblTotal.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(4)))), ((int)(((byte)(73)))), ((int)(((byte)(128)))));
            this.lblTotal.Location = new System.Drawing.Point(7, 43);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblTotal.Size = new System.Drawing.Size(95, 14);
            this.lblTotal.TabIndex = 43;
            this.lblTotal.Text = "Trans Count :";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.Filter = "\"txt files (*.txt)|*.txt|All files (*.*)|*.*\";";
            this.openFileDialog1.RestoreDirectory = true;
            this.openFileDialog1.Title = "Browse Transaction File";
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ShowtoolStripMenuItem,
            this.HidetoolStripMenuItem,
            this.ExittoolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(104, 70);
            // 
            // ShowtoolStripMenuItem
            // 
            this.ShowtoolStripMenuItem.Image = global::SaaSPushTech.Properties.Resources.Show;
            this.ShowtoolStripMenuItem.Name = "ShowtoolStripMenuItem";
            this.ShowtoolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.ShowtoolStripMenuItem.Text = "Show";
            this.ShowtoolStripMenuItem.Click += new System.EventHandler(this.ShowtoolStripMenuItem_Click);
            // 
            // HidetoolStripMenuItem
            // 
            this.HidetoolStripMenuItem.Image = global::SaaSPushTech.Properties.Resources.Hide;
            this.HidetoolStripMenuItem.Name = "HidetoolStripMenuItem";
            this.HidetoolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.HidetoolStripMenuItem.Text = "Hide";
            this.HidetoolStripMenuItem.Click += new System.EventHandler(this.HidetoolStripMenuItem_Click);
            // 
            // ExittoolStripMenuItem
            // 
            this.ExittoolStripMenuItem.Image = global::SaaSPushTech.Properties.Resources.Exit;
            this.ExittoolStripMenuItem.Name = "ExittoolStripMenuItem";
            this.ExittoolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.ExittoolStripMenuItem.Text = "Exit";
            this.ExittoolStripMenuItem.Click += new System.EventHandler(this.ExittoolStripMenuItem_Click);
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.BalloonTipText = "SaaS PushTech";
            this.notifyIcon1.BalloonTipTitle = "SaaS PushTech";
            this.notifyIcon1.ContextMenuStrip = this.contextMenuStrip1;
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Text = "SaaS PushTech";
            this.notifyIcon1.Visible = true;
            this.notifyIcon1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.notifyIcon1_MouseDoubleClick);
            // 
            // pnlTop
            // 
            this.pnlTop.BackColor = System.Drawing.Color.Transparent;
            this.pnlTop.BackgroundImage = global::SaaSPushTech.Properties.Resources.header;
            this.pnlTop.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlTop.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlTop.Controls.Add(this.Label1);
            this.pnlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTop.Location = new System.Drawing.Point(0, 0);
            this.pnlTop.Name = "pnlTop";
            this.pnlTop.Size = new System.Drawing.Size(230, 31);
            this.pnlTop.TabIndex = 51;
            // 
            // Label1
            // 
            this.Label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Label1.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label1.ForeColor = System.Drawing.Color.White;
            this.Label1.Location = new System.Drawing.Point(0, 0);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(228, 29);
            this.Label1.TabIndex = 1;
            this.Label1.Text = "Push Data Service";
            this.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cmdCloseNetWork
            // 
            this.cmdCloseNetWork.BackColor = System.Drawing.Color.Transparent;
            this.cmdCloseNetWork.BackgroundImage = global::SaaSPushTech.Properties.Resources.delete;
            this.cmdCloseNetWork.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.cmdCloseNetWork.Cursor = System.Windows.Forms.Cursors.Default;
            this.cmdCloseNetWork.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdCloseNetWork.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdCloseNetWork.ForeColor = System.Drawing.Color.White;
            this.cmdCloseNetWork.Location = new System.Drawing.Point(371, 38);
            this.cmdCloseNetWork.Name = "cmdCloseNetWork";
            this.cmdCloseNetWork.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cmdCloseNetWork.Size = new System.Drawing.Size(134, 25);
            this.cmdCloseNetWork.TabIndex = 39;
            this.cmdCloseNetWork.Text = "Close Network";
            this.cmdCloseNetWork.UseVisualStyleBackColor = false;
            this.cmdCloseNetWork.Click += new System.EventHandler(this.cmdCloseNetWork_Click);
            // 
            // cmdOpenNetWork
            // 
            this.cmdOpenNetWork.BackColor = System.Drawing.Color.Transparent;
            this.cmdOpenNetWork.BackgroundImage = global::SaaSPushTech.Properties.Resources.update;
            this.cmdOpenNetWork.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.cmdOpenNetWork.Cursor = System.Windows.Forms.Cursors.Default;
            this.cmdOpenNetWork.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdOpenNetWork.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdOpenNetWork.ForeColor = System.Drawing.Color.White;
            this.cmdOpenNetWork.Location = new System.Drawing.Point(231, 38);
            this.cmdOpenNetWork.Name = "cmdOpenNetWork";
            this.cmdOpenNetWork.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cmdOpenNetWork.Size = new System.Drawing.Size(134, 25);
            this.cmdOpenNetWork.TabIndex = 38;
            this.cmdOpenNetWork.Text = "Open Network";
            this.cmdOpenNetWork.UseVisualStyleBackColor = false;
            this.cmdOpenNetWork.Click += new System.EventHandler(this.cmdOpenNetWork_Click);
            // 
            // FrmLog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(230, 69);
            this.Controls.Add(this.pnlTop);
            this.Controls.Add(this.lblTotal);
            this.Controls.Add(this.cmdCloseNetWork);
            this.Controls.Add(this.cmdOpenNetWork);
            this.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmLog";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MINOP PushData V 1.0(1022)";
            this.WindowState = System.Windows.Forms.FormWindowState.Minimized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmLog_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmLog_FormClosed);
            this.Load += new System.EventHandler(this.FrmLog_Load);
            this.contextMenuStrip1.ResumeLayout(false);
            this.pnlTop.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.Label lblTotal;
        public System.Windows.Forms.Button cmdCloseNetWork;
        public System.Windows.Forms.Button cmdOpenNetWork;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Panel pnlTop;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem ShowtoolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem HidetoolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ExittoolStripMenuItem;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
        internal System.Windows.Forms.Label Label1;
        //private AxFPCLOCK_SVRLib.AxFPCLOCK_Svr AxFPCLOCK_Svr1;
    }
}

