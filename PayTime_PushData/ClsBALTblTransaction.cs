using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace SaaSPushTech
{
    public class ClsBALTblTransaction
    {
        #region "Declaration"

        private Int64 _EnrollNo;
        public Int64 EnrollNo
        {
            get { return _EnrollNo; }
            set { _EnrollNo = value; }
        }

        private DateTime _PunchTime;
        public DateTime PunchTime
        {
            get { return _PunchTime; }
            set { _PunchTime = value; }
        }

        private string _VerifyMode;
        public string VerifyMode
        {
            get { return _VerifyMode; }
            set { _VerifyMode = value; }
        }

        private string _InOutMode;
        public string InOutMode
        {
            get { return _InOutMode; }
            set { _InOutMode = value; }
        }

        private string _DeviceIP;
        public string DeviceIP
        {
            get { return _DeviceIP; }
            set { _DeviceIP = value; }
        }

        private Int64 _DeviceProt;
        public Int64 DeviceProt
        {
            get { return _DeviceProt; }
            set { _DeviceProt = value; }
        }

        private Int64 _DeviceID;
        public Int64 DeviceID
        {
            get { return _DeviceID; }
            set { _DeviceID = value; }
        }

        private Int64 _ErrorCode;
        public Int64 ErrorCode
        {
            get { return _ErrorCode; }
            set { _ErrorCode = value; }
        }

        private string _ErrorDesc;
        public string ErrorDesc
        {
            get { return _ErrorDesc; }
            set { _ErrorDesc = value; }
        }
        private string _DeviceSerialNo;
        public string DeviceSerialNo
        {
            get { return _DeviceSerialNo; }
            set { _DeviceSerialNo = value; }
        }
        private string _token;
        public string token
        {
            get { return _token; }
            set { _token = value; }
        }


        ClsDALTransaction ObjClsDALTransaction = new ClsDALTransaction();

        #endregion

        #region "Functions"

        public int OnSave(ref string ErrorMsg)
        {
            ErrorMsg = "";
            try
            {
                return ObjClsDALTransaction.OnSave(this);
            }
            catch (Exception ex)
            {
                ErrorMsg = ex.Message;
                ClsGlobal.WriteErrorLog("OnSave BAL : " + ex.Message);
                return -2;
            }
        }
        public void SayHello(string DeviceID, ref string ErrorMsg)
        {
            ErrorMsg = "";
            try
            {
                ObjClsDALTransaction.SayHello(DeviceID);
            }
            catch (Exception ex)
            {
                ErrorMsg = ex.Message;
                ClsGlobal.WriteErrorLog("SayHello BAL : " + DeviceID + " " + ex.Message);
            }
        }
        //public DataTable GetServerPort()
        //{
        //    try
        //    {
        //        return ObjClsDALTransaction.GetServerPort(this);
        //    }
        //    catch (Exception ex)
        //    {
        //        ClsGlobal.WriteErrorLog("GetServerPort BAL : " + ex.Message);
        //        return null;
        //    }
        //}

        #endregion
    }
    public class MRespo
    {
        public string MegSts { get; set; }
        public string Meg { get; set; }
    }
}
